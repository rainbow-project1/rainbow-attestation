# RAINBOW attestation toolkit

This repository contains RAINBOW's privacy-respecting attestation toolkit, which includes two main components: (i) a Configuration Integrity Verification (CIV) component, and (ii) a Control-Flow Attestation (CFA) component (iii) Key managament APIs with a small example how to use it and an autoinstallation script for a Hardware TPM attached to a Raspberry Pi and (iv) Attestation Protovol Verifiation (APV) models for ProVerif.

## Disclaimer

All implementations are only research prototypes.

from tpm2_pytss import *
from tpm2_pytss.types import *
import templates as tp
import hashlib
import openssl_utils
import utils

def auth_counter_index(ectx, nvCounterIndex):

	KH_EpK, EpK, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_ephemeral_key_templ(), primaryHandle = ESYS_TR.NULL)

	Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(EpK.getName())).digest()).digest()

	secret_value = b"Donne"

	nvpub = TPM2B_NV_PUBLIC(
		nvPublic=TPMS_NV_PUBLIC(
			nvIndex=nvCounterIndex,
			nameAlg=TPM2_ALG.SHA256,
			attributes= TPMA_NV.NO_DA | (TPM2_NT.PIN_PASS << TPMA_NV.TPM2_NT_SHIFT) | TPMA_NV.POLICYWRITE | TPMA_NV.POLICYREAD,
			authPolicy=Pd,
			dataSize=8,
			)
		)

	ACI_handle = ectx.NV_DefineSpace(secret_value, nvpub)

	hashed = hashlib.sha256(b"\x00\x00\x00\x00").digest()

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

	authSignature = ectx.sign(KH_EpK, hashed, scheme, validation)

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	timeout, policy_ticket = ectx.policy_signed(
			KH_EpK, policySession, b"", b"", b"", 0, authSignature
		)

	data = b'\x00\x00\x00\x00\x00\x00\x00\x02'
	ectx.NV_Write(ACI_handle, data, offset = 0, authHandle = ACI_handle, session1 = policySession)

	ectx.FlushContext(policySession)
	ectx.FlushContext(KH_EpK)

	return ACI_handle



def init_revocation_index(ectx, ACI_handle):

	aci_public, aci_name = ectx.NV_ReadPublic(ACI_handle)

	Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x51"+bytes(aci_name)).digest()).digest()

	inP = tp.get_ephemeral_key_templ()
	inP.publicArea.authPolicy = Pd

	KH_AK, AK_pub, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), inP)

	Rev_Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x6a"+bytes(AK_pub.getName())).digest()).digest()

	ectx.FlushContext(KH_AK)

	revIndex_pub = TPM2B_NV_PUBLIC(
		nvPublic=TPMS_NV_PUBLIC(
			nvIndex=0x01001011,
			nameAlg=TPM2_ALG.SHA256,
			attributes=TPMA_NV.POLICYWRITE | TPMA_NV.OWNERREAD | TPMA_NV.NO_DA | (TPM2_NT.BITS << TPMA_NV.TPM2_NT_SHIFT),
			authPolicy=Rev_Pd,
			dataSize=8,
			)
		)

	Revocation_handle = ectx.NV_DefineSpace(b"", revIndex_pub)

	return Revocation_handle



def get_final_pol_digest(ectx, Revocation_handle, AK_pub, ra_pk_name, bits):

	public_final_rev, name_final_rev = ectx.NV_ReadPublic(Revocation_handle)

	public_final_rev.nvPublic.attributes |= TPMA_NV.WRITTEN 

	nvNameWritten = public_final_rev.getName()

	Beta = []

	cpHash = []

	policyDigests = []

	subDigests = []

	allDigests = []

	finalDigests = []

	# for more revocation indexes we propably need an anonymizer counter

	# Activation branch digest
	b0 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(AK_pub.getName())).digest()).digest()

	# Beta.append(b0)
	# subDigests.append(b0)
	finalDigests.append(b0)

	for index, bit in enumerate(bits):

		b1, b2 = bytearray(32), bytearray(32)

		S_data, H_data = bytearray(8), bytearray(8)

		div1, mod1 = divmod(bit[0], 8)

		# Each psedon has its own S_data

		S_data[7 - div1] |= 1 << mod1

		div2, mod2 = divmod(bit[1], 8)

		H_data[7 - div1] |= 1 << mod1
		H_data[7 - div2] |= 1 << mod2

		# softRevocationDigest
		Hs = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(nvNameWritten)+bytes(nvNameWritten) + S_data).digest()

		# hardRevocationDigest
		Hh = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(nvNameWritten)+bytes(nvNameWritten) + H_data).digest() 

		b1 = hashlib.sha256(hashlib.sha256(b1+b"\x00\x00\x01\x60"+bytes(ra_pk_name)).digest()).digest()

		b1 = hashlib.sha256(b1+b"\x00\x00\x01\x6e"+Hs).digest()

		b2 = hashlib.sha256(hashlib.sha256(b2+b"\x00\x00\x01\x60"+bytes(ra_pk_name)).digest()).digest()

		b2 = hashlib.sha256(b2+b"\x00\x00\x01\x6e"+Hh).digest()

		policyDigests.append((Hs, Hh))

		cpHash.append((b1, b2))

		b = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71" + b1 + b2).digest()

		Beta.append(b)

		if (len(Beta) == 8 or index == len(bits)-1):
			sub_b = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71" + b''.join(Beta)).digest()
			subDigests.append(sub_b)
			allDigests.append(Beta)
			Beta = []

	sub_all = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71" + b''.join(subDigests)).digest()
	finalDigests.append(sub_all)

	finalPolicy = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71"+ b''.join(finalDigests)).digest()

	return finalPolicy, name_final_rev, allDigests, cpHash, policyDigests, subDigests, finalDigests



def activate_revoc_index(ectx, finalPolicy, name_final_rev, KH_AK, AK_pub, finalDigests, Revocation_handle, ACI_handle):

	Hp = hashlib.sha256(finalPolicy).digest()

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	timeout, policyTicket = ectx.policy_secret(ACI_handle, policySession, b"", b"", b"", 0)

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

	Sp = ectx.sign(KH_AK, Hp, scheme, validation, session1 = policySession)

	ectx.FlushContext(policySession)

	Hcp = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(name_final_rev)+bytes(name_final_rev) + bytearray(8)).digest()

	# Calculate Hcp for revoked bit

	Ho = hashlib.sha256(b"\x00\x00\x00\x00" + Hcp).digest()

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	timeout, policyTicket = ectx.policy_secret(ACI_handle, policySession, b"", b"", b"", 0)

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

	So = ectx.sign(KH_AK, Ho, scheme, validation, session1 = policySession)

	ectx.FlushContext(policySession)

	t_verified = ectx.verify_signature(KH_AK, Hp, Sp)

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	timeout, policy_ticket = ectx.policy_signed(
			KH_AK, policySession, b"", Hcp, b"", 0, So
		)

	ectx.policy_or(policySession, finalDigests)

	AKname = AK_pub.getName()

	ectx.PolicyAuthorize(policySession, finalPolicy, b"", AKname, t_verified)

	ectx.FlushContext(KH_AK)

	ectx.NV_SetBits(Revocation_handle, 0, Revocation_handle, session1 = policySession)

	ectx.FlushContext(policySession)

	return AKname, t_verified



def getPolicyNVDigest(ectx, SRbit, HRbit, Revocation_handle):

	public_final_rev, name_final_rev = ectx.NV_ReadPublic(Revocation_handle) 

	nvNameWritten = public_final_rev.getName()

	args = bytearray(12)

	div, mod = divmod(SRbit, 8)

	args[7 - div] |= 1 << mod

	div, mod = divmod(HRbit, 8)

	args[7 - div] |= 1 << mod

	args[11] = 0x0b

	argsHash = hashlib.sha256(args).digest()

	policyDigest = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x49" + argsHash + bytes(nvNameWritten)).digest()

	return policyDigest



def generate_and_sign(ectx, sign_key_handle, policyDigests):

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256

	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)
	
	SoftRevocationSig = []
	HardRevocationSig = []

	for ps_t in policyDigests:

		Ho = hashlib.sha256(b"\x00\x00\x00\x00" + ps_t[0]).digest()

		temp = ectx.sign(sign_key_handle, Ho, scheme, validation)

		SoftRevocationSig.append((bytes(temp.signature.ecdsa.signatureR.buffer), bytes(temp.signature.ecdsa.signatureS.buffer)))

		Ho = hashlib.sha256(b"\x00\x00\x00\x00" + ps_t[1]).digest()

		temp = ectx.sign(sign_key_handle, Ho, scheme, validation)

		HardRevocationSig.append((bytes(temp.signature.ecdsa.signatureR.buffer), bytes(temp.signature.ecdsa.signatureS.buffer)))

	return SoftRevocationSig, HardRevocationSig


def sign_msg_with_peud(ectx, digest_to_sign, Revocation_handle, SRBit, HRBit, ps_handle, pt_psk, Points, msg):

	data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	counter = int.from_bytes(data.buffer, byteorder = "big")
	counter = "{0:064b}".format(counter)

	if((counter[len(counter)-1] == "0") and (counter[len(counter)- SRBit-1] == "0")):

		sym = TPMT_SYM_DEF(
				algorithm=TPM2_ALG.XOR,
				keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
				mode=TPMU_SYM_MODE(TPM2_ALG.NULL),
			)

		policySession = ectx.start_auth_session(
				ESYS_TR.NONE,
				ESYS_TR.NONE,
				None,
				TPM2_SE.POLICY,
				sym,
				TPM2_ALG.SHA256,
			)

		operand = bytearray(8)

		div, mod = divmod(SRBit, 8)

		operand[7 - div] |= 1 << mod

		div, mod = divmod(HRBit, 8)

		operand[7 - div] |= 1 << mod

		# operation = b'0x000B'
		operation = 11

		ectx.PolicyNV(ESYS_TR.OWNER, Revocation_handle, policySession, bytes(operand), operation)

		scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
		scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
		validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

		signature = ectx.sign(ps_handle, digest_to_sign, scheme, validation, session1 = policySession)

		ectx.FlushContext(policySession)

		#########################################################
		# 2nd way for verification, one is gonna stay (this one)
		#########################################################

		# sig_r = bytes(signature.signature.ecdaa.signatureR.buffer)
		# sig_s = bytes(signature.signature.ecdaa.signatureS.buffer)

		# Scm = ["1", sig_s, sig_r]

		# curve = openssl_utils.get_BNP256_curve()

		# openssl_utils.check_curve(curve)

		# rc = openssl_utils.point_is_on_curve(curve, pt_psk)
		# if (rc != 1):
		# 	raise Exception("The ECDSA public key is not on the curve")

		# daa_verified = openssl_utils.psk_sign_verification(curve, pt_psk, Scm, digest_to_sign)

		# daa_verified = openssl_utils.psk_DAA_verify(curve, Scm, msg, Points)

		# if (daa_verified != 1):
		# 	print("Signature verification Failed")

		return signature
	else:
		print("The pseudonym is revoked and can't be ued for signing")
		return -1


def sign_msg_with_daa(ectx, msg, daa_handle):

	_, _, _, counter = ectx.commit(daa_handle, TPM2B_ECC_POINT(), TPM2B_SENSITIVE_DATA(), TPM2B_ECC_PARAMETER())

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDAA)
	scheme.details.ecdaa.hashAlg = TPM2_ALG.SHA256
	scheme.details.ecdaa.count = counter
	# validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.ENDORSEMENT)

	digest = hashlib.sha256(msg).digest()

	digest_to_sign, validation = ectx.hash(digest, TPM2_ALG.SHA256, ESYS_TR.ENDORSEMENT)

	signature = ectx.sign(daa_handle, digest_to_sign, scheme, validation)

	return signature


def revoke(ectx, sign_public, Revocation_handle, signature, pol_digest, policyDigests, cpHash, allDigests, subDigests, finalDigests, finalPolicy, AKname, t_verified, TPM_status):


	public_final_rev, name_final_rev = ectx.NV_ReadPublic(Revocation_handle)

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256

	temp_sign = TPMT_SIGNATURE()
	temp_sign.sigAlg = scheme.scheme
	temp_sign.signature = TPMU_SIGNATURE()
	temp_sign.signature.ecdsa = TPMS_SIGNATURE_ECC()
	temp_sign.signature.ecdsa.hash = TPM2_ALG.SHA256

	temp_sign.signature.ecdsa.signatureR = signature[0]
	temp_sign.signature.ecdsa.signatureS = signature[1]

	# t_verified = ectx.verify_signature(sign_handle, cp_hash, temp_sign)

	#######################################################################

	Sbit = -1
	for j in range(2):
		for i in range(len(policyDigests)):
			if (pol_digest == policyDigests[i][j]):
				Sbit = i+1
				Hbit = 0
				Rtype = j
				break

	if(Sbit == -1):
		print("Message is not for us")
		return 0, TPM_status

	sign_handle = ectx.load_external(None, sign_public)

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	timeout, policy_ticket = ectx.policy_signed(
			sign_handle, policySession, b"", pol_digest, b"", 0, temp_sign
		)


	ectx.PolicyCpHash(policySession, pol_digest)

	temp = [cpHash[Sbit-1][0], cpHash[Sbit-1][1]]

	ectx.policy_or(policySession, temp)

	Beta = allDigests[(Sbit-1)//8]

	ectx.policy_or(policySession, Beta)

	ectx.policy_or(policySession, subDigests)

	ectx.policy_or(policySession, finalDigests)

	ectx.PolicyAuthorize(policySession, finalPolicy, b"", AKname, t_verified)

	bits = 0 

	if(Rtype == 0):

		bits |= 1 << Sbit

		ectx.NV_SetBits(Revocation_handle, bits, Revocation_handle, session1 = policySession)
		print("pseudonym was revoked successfully")

	elif(Rtype == 1):

		bits |= 1 << Sbit

		bits |= 1 << Hbit

		ectx.NV_SetBits(Revocation_handle, bits, Revocation_handle, session1 = policySession)
		print("TPM was hard revoked")
		TPM_status = 1

	ectx.FlushContext(policySession)

	ectx.FlushContext(sign_handle)

	return 1, TPM_status

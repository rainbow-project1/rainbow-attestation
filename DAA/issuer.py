from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import pickle
import utils
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import templates as tp
import openssl_utils
import utils
import socket
import communication_utils as coms

def issuer_verifies_resp(curve, K1_box, Sch):

	pk_x, pk_y = utils.get_iso_point()

	string = b"".join(pk_x[0]) + b"".join(pk_x[1]) + b"".join(pk_y[0]) + b"".join(pk_y[1]) + K1_box

	string += bytes(ek_public.publicArea.unique.rsa.buffer)
		
	v = Sch[0]
	w = Sch[1]

	w_p1_bb = openssl_utils.ec_generator_mul(curve, w)

	rc = openssl_utils.point_is_on_curve(curve, w_p1_bb)
	if (rc != 1):
		raise Exception("[w]P_1 is not on curve")

	v_q2_bb = openssl_utils.ec_point_mul(curve, v, daa_public_key)

	rc = openssl_utils.point_is_on_curve(curve, v_q2_bb)
	if (rc != 1):
		raise Exception("[v]Q_2 is not on curve")

	tmp_bb = openssl_utils.ec_point_invert(curve, v_q2_bb)
	e_prime_bb = openssl_utils.ec_point_add(curve, w_p1_bb, tmp_bb)

	rc = openssl_utils.point_is_on_curve(curve, e_prime_bb)
	if (rc != 1):
		raise Exception("U is not on curve")

	# TODO: check if the E point shoud be added last
	pp = hashlib.sha256(utils.get_bnp_P1() + b"".join(daa_public_key) + b"".join(e_prime_bb) + string).digest()
	pp_tpm = hashlib.sha256(pp).digest()

	nj = Sch[2]

	temp_hash = hashlib.sha256(nj + pp_tpm).hexdigest()
	v_prime = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	if (v == v_prime):
		print("Verified Response OK")
		
	else:
		raise SystemExit("verified Failed")


def issuer_creates_AK_creds(curve):

	rc = openssl_utils.point_is_on_curve(curve, daa_public_key)
	if (rc != 1):
		raise Exception("Qs is not on curve")

	nonce_random_bytes = get_random_bytes(4 * 32)

	temp = nonce_random_bytes[:32]
	r = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	temp = nonce_random_bytes[32:64]
	x = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	temp = nonce_random_bytes[64:96]
	y = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	p1 = (b"\x01", b"\x02")
	cre = []

	cre.append(openssl_utils.ec_point_mul(curve, r, p1)) #point A
	cre.append(openssl_utils.ec_point_mul(curve, y, cre[0])) #point B

	ry = utils.int_to_bytes((int.from_bytes(r, "big") * int.from_bytes(y, "big")) % utils.BNP256_ORDER)

	temp = openssl_utils.ec_point_mul(curve, ry, daa_public_key)

	cre.append(openssl_utils.ec_point_mul(curve, x, temp)) #point C
	cre.append(temp) #point D

	temp = nonce_random_bytes[96:128]
	l = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	R_b = openssl_utils.ec_point_mul(curve, l, p1)
	R_d = openssl_utils.ec_point_mul(curve, l, daa_public_key)

	p1 = utils.get_bnp_P1()

	u = hashlib.sha256(p1 + b"".join(daa_public_key) + b"".join(b"".join(p) for p in cre) + \
						b"".join(R_b) + b"".join(R_d)).digest()

	j = int.from_bytes(l, "big") + (int.from_bytes(ry, "big") * int.from_bytes(u, "big") % utils.BNP256_ORDER)
	j = utils.int_to_bytes(j)

	s_cre = (u, j)

	C_ = pickle.dumps([cre, s_cre])

	K2 = get_random_bytes(16)
	cipher = AES.new(K2, AES.MODE_CFB, iv = b'\x00' * 16)
	ct_bytes = cipher.encrypt(C_)

	credblob, secret = ectx.make_credential(ek_handle, K2, daa_name)

	return [bytes(credblob), bytes(secret), ct_bytes]


def issuer_verifies_signature(curve, Scm, Points, message):

	sig_s = Scm[1]
	hash2 = Scm[0]

	s_j_bb = openssl_utils.ec_point_mul(curve, sig_s, Points[4])
	res = openssl_utils.point_is_on_curve(curve, s_j_bb)

	if (res != 1):
		raise Exception("[s]J is not on the curve")

	h2_k_bb = openssl_utils.ec_point_mul(curve, hash2, Points[5])
	res = openssl_utils.point_is_on_curve(curve, h2_k_bb)

	if (res != 1):
		raise Exception("[h_2]K is not on the curve")

	tpm_bb = openssl_utils.ec_point_invert(curve, h2_k_bb)
	l_prime_bb = openssl_utils.ec_point_add(curve, s_j_bb, tpm_bb)
	res = openssl_utils.point_is_on_curve(curve, l_prime_bb)

	if (res != 1):
		raise Exception("L is not on the curve")

	s_pt_s_bb = openssl_utils.ec_point_mul(curve, sig_s, Points[1])
	res = openssl_utils.point_is_on_curve(curve, s_pt_s_bb)

	if (res != 1):
		raise Exception("[s]S is not on the curve")

	h2_pt_w_bb = openssl_utils.ec_point_mul(curve, hash2, Points[3])
	res = openssl_utils.point_is_on_curve(curve, h2_pt_w_bb)

	if (res != 1):
		raise Exception("[h_2]W is not on the curve")

	tpm_bb = openssl_utils.ec_point_invert(curve, h2_pt_w_bb)
	e_prime_bb = openssl_utils.ec_point_add(curve, s_pt_s_bb, tpm_bb)
	res = openssl_utils.point_is_on_curve(curve, e_prime_bb)

	if (res != 1):
		raise Exception("E is not on the curve")

	md_ = hashlib.sha256(message).digest()
	c_ = hashlib.sha256(md_ + b"".join(b"".join(p) for p in Points) + \
							b"".join(l_prime_bb) + b"".join(e_prime_bb)).digest()

	h1_ = hashlib.sha256(c_).digest()

	nm = Scm[2]

	temp_hash = hashlib.sha256(nm + h1_).hexdigest()
	h2_ = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	if (h2_ == hash2):
		print("Signature verification OK")
	else:
		raise SystemExit("Signature verification Failed")





cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2325,host=172.17.0.1")

sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("[*]SOCKET: socket created successfully")
HOST, PORT = "localhost", 9993
sd.bind((HOST, PORT))
print("[*]SOCKET: socket binded successfully")
sd.listen()
print("[*]SOCKET: socket is listening")

with ESAPI(tcti) as ectx:

	ectx.startup(TPM2_SU.CLEAR)

	while(True):

		try:
			# Socket
			c, addr = sd.accept()

			data = c.recv(4096)
			ek_pub, daa_name, tpm_pt_x, tpm_pt_y = pickle.loads(data)
			# End Socket

			ek_public = tp.load_ek_pem(ek_pub)

			ek_handle = ectx.load_external(None, ek_public)

			daa_public_key = (tpm_pt_x, tpm_pt_y)

			K1 = get_random_bytes(16)

			credblob, secret = ectx.make_credential(ek_handle, K1, daa_name)

			# Socket
			arr=[]
			arr.append(bytes(credblob))
			arr.append(bytes(secret))
			data = pickle.dumps(arr)

			coms.box_client(data)
			# End Socket

			print("[*]SOCKET: make_credential finished and waiting for Sch and K1!") 

			# Socket
			c, addr = sd.accept()

			data = c.recv(4096)
			K1_box, data_prime = pickle.loads(data)
			Sch = pickle.loads(data_prime)
			c.close()
			# End Socket

			if (K1 != K1_box):
				raise Exception("K1 not verified")

			curve = openssl_utils.get_BNP256_curve()

			openssl_utils.check_curve(curve)

			issuer_verifies_resp(curve, K1_box, Sch)

			print("Issuer has verified the response from Host")

			to_box = issuer_creates_AK_creds(curve)

			print("Issuer created AK creds")

			# Socket
			data = pickle.dumps(to_box)

			coms.box_client(data)
			# End Socket

			print("[*]SOCKET: Waiting for Host's verification of AK creds + DAA Sign Phase")

			# Socket
			c, addr = sd.accept()

			data = c.recv(4096)
			
			Scm_prime, Points_prime, message_prime = pickle.loads(data)

			Scm = pickle.loads(Scm_prime)
			Points = pickle.loads(Points_prime)
			message = pickle.loads(message_prime)
			c.close()
			# End Socket

			issuer_verifies_signature(curve, Scm, Points, message)

			# Ask Thanassis what needs to be done if verification fails

			print("Issuer has verified the signature")

			ectx.FlushContext(ek_handle)

		except KeyboardInterrupt:
			
			sd.close()
from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import random
import templates as tp
import pickle
import revocation_utils
import socket
import communication_utils as coms
# from flask import Flask
from _thread import *
import threading

def box1(data):
	try:
		sd1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		HOST1, PORT1 = "localhost", 9998

		sd1.connect((HOST1, PORT1))

		data = pickle.dumps(data)

		sd1.sendall(data)

		data = sd1.recv(4096)

		return data

	except:
		sd1.close()

def box2(data):
	try:
		sd_box2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		HOST2, PORT2 = "localhost", 9991

		sd_box2.connect((HOST2, PORT2))

		data = pickle.dumps(data)

		sd_box2.sendall(data)

		data = sd_box2.recv(4096)

		sd_box2.close()

		return data

	except:
		sd_box2.close()

# def threadFunc(c):

# 	while True:
# 		# Socket
# 		data = pickle.dumps(signing_key_public.toPEM())

# 		c.sendall(data)
# 		# End Socket

# 		c.close()
					
# 		# Socket

# 		c, addr = sd.accept()
# 		data = c.recv(4096)

# 		print("[*]SOCKET: Waiting for box to send us the key's cpHashed...")

# 		print("Now we are registering the signatures of the cpHashes of the box keys...")

# 		policyDigests, Signed_cpHashes = pickle.loads(data)
				
# 		# End Socket
				
# 		SoftRevocationSigs, HardRevocationSigs = revocation_utils.generate_and_sign(ectx, sign_key_handle, policyDigests)

# 		# Socket
# 		arr = []

# 		arr.append(SoftRevocationSigs)
# 		arr.append(HardRevocationSigs)

# 		data = pickle.dumps(arr)

# 		c.sendall(data)

# 	c.close()

cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2327,host=172.0.0.1")

sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("[*]SOCKET: socket created successfully")
HOST, PORT = "localhost", 9995
sd.bind((HOST, PORT))
print("[*]SOCKET: socket binded successfully")
sd.listen()
print("[*]SOCKET: socket is listening")

with ESAPI(tcti) as ectx:

	ectx.startup(TPM2_SU.CLEAR)

	root_handle, root_public, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_ra_root_templ(), primaryHandle = ESYS_TR.NULL)

	sign_priv, sign_public, _, _, _ = ectx.create(root_handle, TPM2B_SENSITIVE_CREATE(), tp.get_ra_pk_templ())

	sign_key_handle = ectx.load(root_handle, sign_priv, sign_public)

	ectx.FlushContext(root_handle)

	print(f"The sign key handle is at: {hex(sign_key_handle)}")

	signing_key_public = sign_public


	revocation_map = {}


	# while (count != 2):
	# Added for new 63 pseud
	while(True):

		try:
			print("[*]SOCKET: Waiting for box to connect")
			c, addr = sd.accept()
			print("[*]SOCKET: Box is now connected")

			# Added for new 63 pseud
			data = c.recv(18364)
			data = pickle.loads(data)

			if(data[0] == "sign_key"):

				data = pickle.dumps(signing_key_public.toPEM())

				c.sendall(data)
				c.close()

			elif(data[0] == "revoke"):

				pseud = data[1]
				Rtype = int(data[2])
				if (pseud in revocation_map.keys()):	
					revocation_data = revocation_map[pseud]

					data = ["revoke", revocation_data[Rtype], revocation_data[2][Rtype]]

					status = box1(data)
					status = pickle.loads(status)
					status1 = box2(data)
					status1 = pickle.loads(status1)

					if(status[0] == "Pseudonym revoked"):

						status = pickle.dumps(status)
						c.sendall(status)
						# c.close()

					else:

						status1 = pickle.dumps(status1)
						c.sendall(status1)
						# c.close()

				else:
					print("The pseudonym does not exist")

				c.close()
						
			# Socket
			# print("[*]SOCKET: Waiting for box to reconnect")
			# c, addr = sd.accept()
			# data = c.recv(18364)

			else:

				print("[*]SOCKET: Waiting for box to send us the key's cpHashed...")

				print("Now we are registering the signatures of the cpHashes of the box keys...")

				# pseud_public, policyDigests, Signed_cpHashes = pickle.loads(data)

				pseud_public = data[0]
				policyDigests = data[1]
				Signed_cpHashes = data[2]
						
				# End Socket
						
				SoftRevocationSigs, HardRevocationSigs = revocation_utils.generate_and_sign(ectx, sign_key_handle, policyDigests)

				# Socket
				arr = []

				arr.append(SoftRevocationSigs)
				arr.append(HardRevocationSigs)

				data = pickle.dumps(arr)

				c.sendall(data)
				c.close()
				# count += 1


				for i in range(len(pseud_public)):
					revocation_map.update({str(pseud_public[i]): (SoftRevocationSigs[i], HardRevocationSigs[i],policyDigests[i])})


		except KeyboardInterrupt:
			
			sd.close()


	# print("We are waiting for revocation calls")

	# while(True):

	# 	c,addr = sd.accept()

	# 	data = c.recv(1024)

	# 	pseud, Rtype = pickle.loads(data)
	# 	Rtype = int(Rtype)
	# 	if (pseud in revocation_map.keys()):	
	# 		revocation_data = revocation_map[pseud]

	# 		data = ["revoke", revocation_data[Rtype], revocation_data[2][Rtype]]

	# 		status = box1(data)
	# 		status = pickle.loads(status)
	# 		status1 = box2(data)
	# 		status1 = pickle.loads(status1)

	# 		if(status[0] == "Pseudonym revoked"):

	# 			status = pickle.dumps(status)
	# 			c.sendall(status)

	# 		else:

	# 			status1 = pickle.dumps(status1)
	# 			c.sendall(status1)

	# 	else:
	# 		print("The pseudonym does not exist")

	# 	c.close()


from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import random
import templates as tp
import pickle
import revocation_utils
import socket
import communication_utils as coms

cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2327,host=172.17.0.1")

sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print("[*]SOCKET: socket created successfully")
HOST, PORT = "localhost", 9995
sd.bind((HOST, PORT))
print("[*]SOCKET: socket binded successfully")
sd.listen()
print("[*]SOCKET: socket is listening")

with ESAPI(tcti) as ectx:

	ectx.startup(TPM2_SU.CLEAR)

	root_handle, root_public, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_ra_root_templ(), primaryHandle = ESYS_TR.NULL)

	sign_priv, sign_public, _, _, _ = ectx.create(root_handle, TPM2B_SENSITIVE_CREATE(), tp.get_ra_pk_templ())

	sign_key_handle = ectx.load(root_handle, sign_priv, sign_public)

	ectx.FlushContext(root_handle)

	print(f"The sign key handle is at: {hex(sign_key_handle)}")

	signing_key_public = sign_public

	while (True):

		try:
			c, addr = sd.accept()

			# Socket
			data = pickle.dumps(signing_key_public.toPEM())

			coms.box_client(data)
			# End Socket

			print("[*]SOCKET: Waiting for box to send us the key's cpHashed...")

			print("Now we are registering the signatures of the cpHashes of the box keys...")
				
			# Socket
			c, addr = sd.accept()

			data = c.recv(9182)

			policyDigests, Signed_cpHashes = pickle.loads(data)
			
			c.close()
			# End Socket
			
			SoftRevocationSigs, HardRevocationSigs = revocation_utils.generate_and_sign(ectx, sign_key_handle, policyDigests)

			# Socket
			arr = []

			arr.append(SoftRevocationSigs)
			arr.append(HardRevocationSigs)

			data = pickle.dumps(arr)

			coms.box_client(data)
			# End Socket
		except KeyboardInterrupt:
			
			sd.close()
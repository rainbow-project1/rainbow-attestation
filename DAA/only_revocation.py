from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import random

cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2321,host=172.17.0.1")

with ESAPI(tcti) as ectx:
	ectx.startup(TPM2_SU.CLEAR)

	#ti einai ta rids[]?
	#ta vgazei o issuer(?) in our case
	#gt exoun ta idio publik kleidi?

	#in /home/alekos/Revocation/main.cpp line 51
	#POIO EINAI TO EPHEMERAL KEY STIN DIKI MAS PERIPTOSI?
	#in /home/alekos/Revocation/source/TpmManager.cpp line 409
	insens = TPM2B_SENSITIVE_CREATE()
	inPublic = TPM2B_PUBLIC()
	inPublic.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inPublic.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inPublic.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	#daa_handle must be used under auth_counter (check in TCG --counter for authorize in a use of a key)
	#in cpp the hierarchy is NULL, here is OWNER
	daa_handle, daa_public, _, _, _ = ectx.CreatePrimary(insens, inPublic)

	#in /home/alekos/Revocation/source/Host.cpp line 231
	#poia einai i xrisi aytou toy policy?
	#nvram must be linked with specific policy and in our case nvram must be linked with the ek
	pol_sign = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(daa_public.getName())).digest()).digest()

	#in /home/alekos/Revocation/source/Host.cpp line 234 
	#to secret index pos proekipse? + ti kanoyn ta attributes?
	#(tpm_.NV_DefinePinPassSpace(ctx_, index_handle, &epehemeralAuthIndexDigest, secret);)
	nvpub = TPM2B_NV_PUBLIC(
		nvPublic=TPMS_NV_PUBLIC(
			nvIndex=0x01010101,
			nameAlg=TPM2_ALG.SHA256,
			attributes= TPMA_NV.NO_DA | TPM2_NT.PIN_PASS | TPMA_NV.POLICYWRITE | TPMA_NV.POLICYREAD,
			authPolicy=pol_sign,
			dataSize=8,
			)
		)

	#stin diki mas periptosi ti tha einai to minima?
	#pin_pass = auth counter
	secret_value = b"Donne"

	nvhandle = ectx.NV_DefineSpace(secret_value, nvpub)
	#till this point 

	#in /home/alekos/Revocation/source/Host.cpp line 237
	hashed = hashlib.sha256(b"\x00\x00\x00\x00").digest()
	# in /home/alekos/Revocation/source/PolicyManager.cpp line 353
	#(return tpm.sign(ctx, exdigest, SHA256_DIGEST_SIZE, keyhandle, policysession);)
	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

	#gt to xreiazomaste to authSignature?
	authSignature = ectx.sign(daa_handle, hashed, scheme, validation)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 242
	#gt stin start_auth_session vazoume xor?
	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp in 243
	#to policy_ticket dn xrisimopoieitai poythena, ara gt tin ekteloyme?
	#(policyManager_.policySigned(ctx_, &policySession, ephemeralKey.objectHandle, authSignatrue, nullptr);)
	timeout, policy_ticket = ectx.policy_signed(
			daa_handle, policySession, b"", b"", b"", 0, authSignature
		)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 245
	#ta data ti simainoyn?
	#(tpm_.NV_WritePin(ctx_, index_handle, secret, &policySession, 2);)
	data = b'00002000'

	#check with thanassis if secret_value is passed with this implementation
	#here the auth counter is implemented
	ectx.NV_Write(nvhandle, data, offset = 0, authHandle = nvhandle, session1 = policySession)

	ectx.FlushContext(daa_handle)
	ectx.FlushContext(policySession)

	##############################initializeRevocationIndex(0x01001011, 0x01010101, "Donne")################################

	#########################ASK THANASSIS if reboot is needed#########################

	# ectx.shutdown()

	# ectx.startup(TPM2_SU.CLEAR)

	#in /home/alekos/Revocation/source/Host.cpp line 318
	#ti simvainei me to flow tis pliroforias?
	aci_public, aci_name = ectx.NV_ReadPublic(nvhandle)

	#in /home/alekos/Revocation/source/Host.cpp line 322
	#ti simainei to pol_secret?
	#policyManager_.digestUpdate_PolicySecret(ACI.nvName, nullptr, authKeyDigest);
	pol_secret = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x51"+bytes(aci_name)).digest()).digest()
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 328+329
	insens2 = TPM2B_SENSITIVE_CREATE()
	inPublic2 = TPM2B_PUBLIC()
	inPublic2.publicArea.authPolicy = pol_secret
	inPublic2.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic2.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic2.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inPublic2.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inPublic2.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inPublic2.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inPublic2.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic2.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic2.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	auth_handle, auth_public, _, _, _ = ectx.CreatePrimary(insens2, inPublic2)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 347
	#afou to kleidi ginetai flush ti noima exei to policy poy ftiaxnetai vasi aytou?
	#policyManager_.digestUpdate_PolicyAuthorize(authorizationKey.name, nullptr, authDigest);
	pol_auth = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x6a"+bytes(auth_public.getName())).digest()).digest()

	ectx.FlushContext(auth_handle)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 353
	#to revocation index pos proekipse? + ti kanoyn ta attributes?
	#sindesi space in nvram me policy author kleidiou
	#tpm_.NV_DefineBitSpace(ctx_, revocation_handle, &authDigest);
	nvpub2 = TPM2B_NV_PUBLIC(
		nvPublic=TPMS_NV_PUBLIC(
			nvIndex=0x01001011,
			nameAlg=TPM2_ALG.SHA256,
			attributes=TPMA_NV.POLICYWRITE | TPMA_NV.OWNERREAD | TPMA_NV.NO_DA | (TPM2_NT.BITS << TPMA_NV.TPM2_NT_SHIFT),
			authPolicy=pol_auth,
			dataSize=8,
			)
		)

	nvhandle_final_rev = ectx.NV_DefineSpace(b"", nvpub2)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 358+359
	#gt to kanoyme read eno molis to kaname define?
	public_final_rev, name_final_rev = ectx.NV_ReadPublic(nvhandle_final_rev)

	#Common::calculateNameWithWrittenSet(nv_public.nvPublic.nvPublic,nvNameWritten);
	#ti rolo paizei?
	public_final_rev.nvPublic.attributes |= TPMA_NV.WRITTEN

	nvNameWritten = public_final_rev.getName()
	#till this point

###################################RA-Create_key_hierarchy###################################

	#in /home/alekos/Revocation/source/RevocationAuthority.cpp line 27
	#ti kleidia einai ayta (root+child) stin periptosi mas?
	insens_root = TPM2B_SENSITIVE_CREATE()
	inPublic_root = TPM2B_PUBLIC()
	inPublic_root.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic_root.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic_root.publicArea.objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.DECRYPT |
		TPMA_OBJECT.RESTRICTED
	)

	inPublic_root.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.AES;
	inPublic_root.publicArea.parameters.eccDetail.symmetric.keyBits.aes = 128;
	inPublic_root.publicArea.parameters.eccDetail.symmetric.mode.aes = TPM2_ALG.CFB;
	inPublic_root.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.NULL;
	inPublic_root.publicArea.parameters.eccDetail.scheme.details.anySig.hashAlg = 0;
	inPublic_root.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic_root.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic_root.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = 0;

	#in /home/alekos/Revocation/source/TpmManager.cpp line 409
	root_handle, root_public, _, _, _ = ectx.CreatePrimary(insens_root, inPublic_root, primaryHandle = ESYS_TR.NULL)

	#in /home/alekos/Revocation/source/TpmManager.cpp line 33
	insens_child = TPM2B_SENSITIVE_CREATE()
	inPublic_child = TPM2B_PUBLIC()
	inPublic_child.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic_child.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic_child.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inPublic_child.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inPublic_child.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inPublic_child.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inPublic_child.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic_child.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic_child.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	#signing_key = tpm_->create_key(ctx_,nullptr,root_key, nullptr, nullptr, &templatePub); (line 35)
	sign_priv, sign_public, _, _, _ = ectx.create(root_handle, insens_child, inPublic_child)
	signing_key_name = sign_public.getName()

	ectx.FlushContext(root_handle)
	#till this point

#######################################GetFinalRevocationPolicy#######################

	#in /home/alekos/Revocation/main.cpp line 65
	#prepei na ftiaksoyme to cleanWriteCommandParameters?
	#vehicle.activateRevocationIndex(rids, NUMBER_OF_RA,SECRET_VALUE,SECRET_INDEX);
	#in /home/alekos/Revocation/source/Host.cpp line 390
	insens3 = TPM2B_SENSITIVE_CREATE()
	inPublic3 = TPM2B_PUBLIC()
	inPublic3.publicArea.authPolicy = pol_secret
	inPublic3.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic3.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic3.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inPublic3.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inPublic3.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inPublic3.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inPublic3.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic3.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic3.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	#in /home/alekos/Revocation/source/Host.cpp line 391
	#ti rolo paizei ayto to kleidi?
	final_rev_handle, final_rev_public, _, _, _ = ectx.CreatePrimary(insens3, inPublic3)

	#in /home/alekos/Revocation/source/Host.cpp line 396
	#policyManager_.digestUpdate_PolicySigned(authorizingKey.name, nullptr,allDigests[NOT_WRITTEN_POLICY]);
	pol_sign2 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(final_rev_public.getName())).digest()).digest()

	ectx.FlushContext(final_rev_handle)
	#till this point

	ra_pk_name = signing_key_name

	bits = ((0, 2), (1,2))
	rid0 = {"revocationAuthorityPK": None, "hardRevocationBit": 2, "softRevocationBit": 0, "softRevocationDigest": None, "hardRevocationDigest": None}
	rid1 = {"revocationAuthorityPK": None, "hardRevocationBit": 2, "softRevocationBit": 1, "softRevocationDigest": None, "hardRevocationDigest": None}
	rids = [rid0, rid1]

	allDigests = []
	allDigests.append(pol_sign2)

	#eksigisi tou flow + policies

	for i,bit in enumerate(bits):

		softBit, hardBit = bit

		rid = rids[i]

		#in /home/alekos/Revocation/source/Common.cpp line 118
		#Common::getCommandParameterHash_WriteBit(rid[i]->softRevocationBit,nvNameWritten,commandParameterHash);
		revocationByte = 0

		revocationByte |= 1 << softBit 	#softRevocationBit assignment

		softRevocationDigest = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(nvNameWritten)+bytes(nvNameWritten) + bytearray((0,0,0,0,0,0,0,revocationByte))).digest()
		#till this point

		rid["softRevocationDigest"] = softRevocationDigest

		#in /home/alekos/Revocation/source/Host.cppl line 423+424
		#policyManager_.digestUpdate_PolicySigned(ra_pk_name,nullptr,b1);
		pol_sign3 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(ra_pk_name)).digest()).digest()

		#policyManager_.digestUpdate_PolicyCPHash(rid[i]->softRevocationDigest, b1);
		pol_cpHash_Soft = hashlib.sha256(pol_sign3 +b"\x00\x00\x01\x6e"+bytes(softRevocationDigest)).digest()
		#till this point

		#in /home/alekos/Revocation/source/Common.cpp line 154
		#Common::getCommandParameterHash_WriteBits(rid[i]->softRevocationBit, rid[i]->hardRevocationBit, nvNameWritten,commandParameterHash);
		revocationByte = 0
		revocationByte |= 1 << softBit #softRevocationBit assignment
		revocationByte |= 1 << hardBit #hardRevocationBit assignment

		hardRevocationDigest = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(nvNameWritten)+bytes(nvNameWritten) + bytearray((0,0,0,0,0,0,0,revocationByte))).digest()
		#till this point 

		rid["hardRevocationDigest"] = hardRevocationDigest

		#in /home/alekos/Revocation/source/Host.cpp line 435+436
		pol_sign4 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(ra_pk_name)).digest()).digest()

		pol_cpHash_Hard = hashlib.sha256(pol_sign4+b"\x00\x00\x01\x6e"+bytes(hardRevocationDigest)).digest()
		#till this point

		#in /home/alekos/Revocation/source/Host.cpp line 442
		#policyManager_.digestUpdate_PolicyOR(beta,2,allDigests[i + 1]);
		pol_or = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71" + pol_cpHash_Soft + pol_cpHash_Hard).digest()
		#till this point

		allDigests.append(pol_or)

	#in /home/alekos/Revocation/source/Host.cpp line 452
	finalDigest = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71"+ b''.join(allDigests)).digest()

#####################after getFinalRevocationPolicy####################################

	#in /home/alekos/Revocation/source/Host.cpp line 562
	#ti kanei?
	digestToSign = hashlib.sha256(finalDigest).digest()

	#in /home/alekos/Revocation/source/Host.cpp line 572
	insens = TPM2B_SENSITIVE_CREATE()
	inPublic = TPM2B_PUBLIC()
	inPublic.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic.publicArea.nameAlg = TPM2_ALG.SHA256
	inPublic.publicArea.authPolicy = pol_secret

	inPublic.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inPublic.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inPublic.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	#in /home/alekos/Revocation/source/Host.cpp line 573
	#ti kleidi einai ayto?
	auth_handle, auth_public, _, _, _ = ectx.CreatePrimary(insens, inPublic)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 576
	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)
	#till this point

	#in /home/alekos/Revocation/source/PolicyManager.cpp line 279
	#policyManager_.policySecret(ctx_, policySession, nullptr, secret_index, secret);
	#we must check whether secret_value should be passed somehow + if nvhandle should be passed
	timeout, policyTicket = ectx.policy_secret(ESYS_TR.OWNER, policySession, b"", b"", b"", 0)
	#till this point
	
	#in /home/alekos/Revocation/source/TpmManager.cpp line 621
	#TPMT_SIGNATURE signedPolicy = tpm_.sign(ctx_, &digestToSign, authorizingKey.objectHandle, &policySession, nullptr);
	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

	signedPolicy = ectx.sign(auth_handle, digestToSign, scheme, validation)#, session1 = policySession)

	ectx.FlushContext(policySession)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 582
	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 583
	timeout, policyTicket = ectx.policy_secret(ESYS_TR.OWNER, policySession, b"", b"", b"", 0)
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 586
	#eksigisi tvn hashes
	#Common::getCommandParameterHash_WriteCleanIndex(nv_public.nvName,write_clear_index_commandParameters)
	cp_hashClean = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(name_final_rev)+bytes(name_final_rev) + bytearray((0,0,0,0,0,0,0,0))).digest()
	#till this point

	#in /home/alekos/Revocation/source/Host.cpp line 588
	#auto signedWrite = policyManager_.generate_and_sign_aHash(tpm_, ctx_, authorizingKey.objectHandle, &policySession, &write_clear_index_commandParameters);
	hashed = hashlib.sha256(b"\x00\x00\x00\x00" + cp_hashClean).digest()

	#in /home/alekos/Revocation/source/PolicyManager.cpp line 353
	#tpm.sign(ctx, exdigest, SHA256_DIGEST_SIZE, keyhandle, policysession);
	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)
	signedWrite = ectx.sign(auth_handle, hashed, scheme, validation)#, session1 = policySession)
	#till this point

	ectx.FlushContext(policySession) #(in Host.cpp line 591)

	#in /home/alekos/Revocation/source/Host.cpp line 595
	# gt xreiazetai i verify/ ti elegxoume?
	#ticket = tpm_.verify_signature(ctx_, digestToSign, &signedPolicy, authorizingKey.objectHandle, verified);
	verified = ectx.verify_signature(auth_handle, digestToSign, signedPolicy)
	#till this pint

	#in Host.cpp line 599
	authName = auth_public.getName()

	#in Host.cpp line 606
	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)
	#till this point

	#in Host.cpp line 607
	#policyManager_.policySigned(ctx_, &policySession, authorizingKey.objectHandle, signedWrite, &write_clear_index_commandParameters);
	timeout, policy_ticket2 = ectx.policy_signed(
			auth_handle, policySession, b"", cp_hashClean, b"", 0, signedWrite
		)
	#till this point

	#in Host.cpp line 608
	#policyManager_.policyOR(ctx_,&policySession,allDigests,digest_count);
	ectx.policy_or(policySession, TPML_DIGEST(allDigests))
	#till this point

	#in Host.cpp line 609
	#policyManager_.policyAuthorize(ctx_,policySession,ticket,authName,finalDigest);
	ectx.PolicyAuthorize(policySession, finalDigest, b"", authName, verified)
	#till this point

	ectx.FlushContext(auth_handle) #(in Host.cpp line 610)

	#in Host.cpp line 613
	#tpm_.NV_SetBit(ctx_,REVOCATION_INDEX,CLEAR,&policySession,true);
	ectx.NV_SetBits(nvhandle_final_rev, 0, nvhandle_final_rev, session1 = policySession)
	#till this point

	ectx.FlushContext(policySession) #(in Host.cpp line 614)

	###############################################################################

	#in main.cpp line 72 (create_key_hierarchy(ra1_keys, 2, 0))

	KeyData1 = {"SealedKey": None, "softRevocationBit": 0, "hardRevocationBit": 0, "proofOfRegistration": None, "localIdentifier": 0}
	KeyData2 = {"SealedKey": None, "softRevocationBit": 0, "hardRevocationBit": 0, "proofOfRegistration": None, "localIdentifier": 0}
	KeyHierarchies = {"keyCount": 0, "keys": [KeyData1, KeyData2], "primary_template": None}

	KeyHierarchies["keyCount"] = 2
	
	insens = TPM2B_SENSITIVE_CREATE()
	inPublic = TPM2B_PUBLIC()
	inPublic.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic.publicArea.objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.DECRYPT |
		TPMA_OBJECT.RESTRICTED
	)

	inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.AES;
	inPublic.publicArea.parameters.eccDetail.symmetric.keyBits.aes = 128;
	inPublic.publicArea.parameters.eccDetail.symmetric.mode.aes = TPM2_ALG.CFB;
	inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.scheme.details.anySig.hashAlg = 0;
	inPublic.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = 0;

	KeyHierarchies["primary_template"] = inPublic

	#in /home/alekos/Revocation/source/TpmManager.cpp line 409
	primaryKey, primary_public, _, _, _ = ectx.CreatePrimary(insens, inPublic, primaryHandle = ESYS_TR.NULL)

	bits = ((0, 2), (1,2))

	for i in range(2):
		# getPolicyNVDigest(info[i]->softRevocationBit, info[i]->hardRevocationBit, policyDigest.b.buffer);
		args = bytearray(12)
		softBit, hardBit = bits[i]
		args[7 - softBit // 8] |= 1 << ((softBit % 8))
		args[7 - hardBit // 8] |= 1 << ((hardBit % 8))
		args[11] = 0x0b

		argHash = hashlib.sha256(args).digest()

		policyDigest = hashlib.sha256(bytes(32) + b"\x00\x00\x01\x49" + argHash + bytes(nvNameWritten)).digest()

		insens = TPM2B_SENSITIVE_CREATE()
		inPublic = TPM2B_PUBLIC()
		inPublic.publicArea.type = TPM2_ALG.ECC
		inPublic.publicArea.nameAlg = TPM2_ALG.SHA256

		inPublic.publicArea.objectAttributes = (
			TPMA_OBJECT.NODA |
			TPMA_OBJECT.SENSITIVEDATAORIGIN |
			TPMA_OBJECT.USERWITHAUTH |
			TPMA_OBJECT.SIGN_ENCRYPT |
			TPMA_OBJECT.ADMINWITHPOLICY
		)

		inPublic.publicArea.authPolicy = policyDigest
		inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
		inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
		inPublic.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
		inPublic.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
		inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
		inPublic.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

		#in hierarchies[hierarchy_no].keys[i].key = tpm_.create_key(ctx_, nullptr, primaryKey, nullptr, nullptr, &keyTemplate); (Host.cpp line 101)

		key_private, key_public, _, _, _ = ectx.create(primaryKey, insens, inPublic)

		KeyHierarchies["keys"][i]["SealedKey"] = (key_private, key_public)

		KeyHierarchies["keys"][i]["hardRevocationBit"] = hardBit
		KeyHierarchies["keys"][i]["softRevocationBit"] = softBit
		KeyHierarchies["keys"][i]["localIdentifier"] = random.randint(0,5000)

	ectx.FlushContext(primaryKey)

	RevocationData = {"beta": [0,0], "hardRevocationSignature": None, "softRevocationSignature": None, "hardRevocationCpHash": None, "softRevocationCpHash": None}
	VehicleKey = {"publicKey": None, "revocationData": RevocationData}

	#in vehicle.hierarchies[0].keys[0].proofOfRegistration = ra1.requestJoin(ra1_keys[0]->softRevocationDigest, ra1_keys[0]->hardRevocationDigest, vehicle.hierarchies[0].keys[0].key.outPublic.publicArea, vehicle.hierarchies[0].keys[0].localIdentifier); (main.cpp line 75)

	VehicleKey["publicKey"] = KeyHierarchies["keys"][0]["SealedKey"][1]

	insens_root = TPM2B_SENSITIVE_CREATE()
	inPublic_root = TPM2B_PUBLIC()
	inPublic_root.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic_root.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic_root.publicArea.objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.DECRYPT |
		TPMA_OBJECT.RESTRICTED
	)

	inPublic_root.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.AES;
	inPublic_root.publicArea.parameters.eccDetail.symmetric.keyBits.aes = 128;
	inPublic_root.publicArea.parameters.eccDetail.symmetric.mode.aes = TPM2_ALG.CFB;
	inPublic_root.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.NULL;
	inPublic_root.publicArea.parameters.eccDetail.scheme.details.anySig.hashAlg = 0;
	inPublic_root.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic_root.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic_root.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = 0;

	#in /home/alekos/Revocation/source/TpmManager.cpp line 409
	root_handle, root_public, _, _, _ = ectx.CreatePrimary(insens_root, inPublic_root, primaryHandle = ESYS_TR.NULL)

	loaded_signing_key = ectx.load(root_handle, sign_priv, sign_public)

	VehicleKey["revocationData"]["hardRevocationCpHash"] = rids[0]["hardRevocationDigest"]
	VehicleKey["revocationData"]["softRevocationCpHash"] = rids[0]["softRevocationDigest"]

	# in k.revocationData.hardRevocationSignature = pm_.generate_and_sign_aHash(*tpm_, ctx_, loaded_signing_key.objectHandle, nullptr, &hardRevocationCpHash); (RevocationAuthority.cpp line 66)

	hashed = hashlib.sha256(b"\x00\x00\x00\x00" + rids[0]["hardRevocationDigest"]).digest()

	#in /home/alekos/Revocation/source/PolicyManager.cpp line 353
	#tpm.sign(ctx, exdigest, SHA256_DIGEST_SIZE, keyhandle, policysession);
	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)
	hardRevocationSignature = ectx.sign(loaded_signing_key, hashed, scheme, validation)

	VehicleKey["revocationData"]["hardRevocationSignature"] = hardRevocationSignature

	hashed = hashlib.sha256(b"\x00\x00\x00\x00" + rids[0]["softRevocationDigest"]).digest()

	#in /home/alekos/Revocation/source/PolicyManager.cpp line 353
	#tpm.sign(ctx, exdigest, SHA256_DIGEST_SIZE, keyhandle, policysession);
	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)
	softRevocationSignature = ectx.sign(loaded_signing_key, hashed, scheme, validation)

	VehicleKey["revocationData"]["softRevocationSignature"] = softRevocationSignature

	name = bytes(VehicleKey["publicKey"].getName()[2:])
	final_name = TPM2B_DIGEST(name)

	#in receipt = tpm_->sign(ctx_,&name,loaded_signing_key.objectHandle, nullptr, nullptr) (RevocationAuthority line 74)
	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)
	receipt = ectx.sign(loaded_signing_key, name, scheme, validation)

	ectx.FlushContext(loaded_signing_key)
	ectx.FlushContext(root_handle)

	b1 = bytearray(32)
	b2 = bytearray(32)

	# in pm_.digestUpdate_PolicySigned(signing_key_name,nullptr,b1); (ReavocationAuthority.cpp line 86)
	b1 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(signing_key_name)).digest()).digest()

	# pm_.digestUpdate_PolicyCPHash(softRevocationCpHash,b1);
	b1 = hashlib.sha256(b1 +b"\x00\x00\x01\x6e"+bytes(VehicleKey["revocationData"]["softRevocationCpHash"])).digest()

	# in pm_.digestUpdate_PolicySigned(signing_key_name,nullptr,b2); (ReavocationAuthority.cpp line 89)
	b2 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(signing_key_name)).digest()).digest()
	b2 = hashlib.sha256(b2 +b"\x00\x00\x01\x6e"+bytes(VehicleKey["revocationData"]["hardRevocationCpHash"])).digest()

	VehicleKey["revocationData"]["beta"][0] = b1
	VehicleKey["revocationData"]["beta"][1] = b2

	# Here we should (?) insert into keystore (ReavocationAuthority.cpp line 98)
	###################

	KeyHierarchies["keys"][0]["proofOfRegistration"] = receipt

	# vehicle.hierarchies[0].keys[1].proofOfRegistration = ra1.requestJoin(ra1_keys[1]->softRevocationDigest, ra1_keys[1]->hardRevocationDigest, vehicle.hierarchies[0].keys[1].key.outPublic.publicArea, vehicle.hierarchies[0].keys[1].localIdentifier); (main.cpp line 76)
	###################

	# ra1.revokeVehicle(vehicle.hierarchies[0].keys[0].localIdentifier,SOFT);
	# ra1.revokeVehicle(vehicle.hierarchies[0].keys[0].localIdentifier,HARD); (line 92-92)

	temp_revocation_data = VehicleKey["revocationData"]

	temp_signature = temp_revocation_data["softRevocationSignature"]

	temp_cpHash = temp_revocation_data["softRevocationCpHash"]

	# in debugHost->revoke(this, *revocationSignature, *revocationHash, r->beta,action,keyIdentifier); (line 123)

	rBit = KeyHierarchies["keys"][0]["softRevocationBit"]
	pBit = KeyHierarchies["keys"][0]["hardRevocationBit"]

	# in LoadExternal_Out loaded_ra_pub = tpm_.load_public(ctx_, ra_pub); (Host.cpp line 184)

	loaded_ra_pub = ectx.load_external(None, sign_public)

	# in tpm_.start_authorization_session(ctx_,&session,TPM_SE_POLICY); (Host.cpp line 188)

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	session = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	# policyManager_.policySigned(ctx_,&session,loaded_ra_pub.objectHandle,signature,&cpHash); 
	timeout, policy_ticket = ectx.policy_signed(
			loaded_ra_pub, session, b"", temp_cpHash, b"", 0, temp_signature
		)

	# policyManager_.policyCpHash(ctx_,&session,cpHash);
	ectx.PolicyCpHash(session, temp_cpHash)

	# policyManager_.policyOR(ctx_, &session, beta, 2);
	ectx.policy_or(session, TPML_DIGEST(VehicleKey["revocationData"]["beta"]))

	# policyManager_.policyOR(ctx_,&session,allDigests,digest_count)
	ectx.policy_or(session, TPML_DIGEST(allDigests))

	# policyManager_.policyAuthorize(ctx_,session,ticket,authName,finalDigest);
	ectx.PolicyAuthorize(session, finalDigest, b"", authName, verified)

	# tpm_.NV_SetBit(ctx_,REVOCATION_INDEX,rBit,&session,false);
	temp = 1 << (rBit)
	ectx.NV_SetBits(nvhandle_final_rev, temp, nvhandle_final_rev, session1 = session)

#########QUESTIONS######################################

# can we use password in intel TSS's ? (it's implmented in ibm's tss)
# Idea: https://github.com/tpm2-software/tpm2-pytss/blob/master/tpm2_pytss/ESAPI.py#L214


# difference between ESAPI and FAPI?
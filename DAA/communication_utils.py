import socket


def issuer_client(data):
	try:
		sd = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		HOST, PORT = "localhost", 9993
		sd.connect((HOST, PORT))
		sd.sendall(data)
		sd.close()
	except:
		sd.close()
		raise Exception("Issuer_client failed")

def RA_client(data):
	try:
		sd = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		HOST, PORT = "localhost", 9995
		sd.connect((HOST, PORT))
		sd.sendall(data)
		sd.close()	
	except:
		sd.close()
		raise Exception("RA_client failed")

def box_client(data):
	try:
		sd = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		HOST, PORT = "localhost", 9998
		sd.connect((HOST, PORT))
		sd.sendall(data)
		sd.close()
	except:
		sd.close()
		raise Exception("Box_client failed")
    @classmethod
    def fromPEM(
        cls,
        data,
        nameAlg=TPM2_ALG.SHA256,
        objectAttributes=(
            TPMA_OBJECT.DECRYPT | TPMA_OBJECT.SIGN_ENCRYPT | TPMA_OBJECT.USERWITHAUTH
        ),
        symmetric=None,
        scheme=None,
        **kwargs,
    ):
        p = cls()
        public_from_encoding(data, p)
        p.nameAlg = nameAlg
        p.objectAttributes = objectAttributes
        if kwargs is None:
            if symmetric is None:
                p.parameters.asymDetail.symmetric.algorithm = TPM2_ALG.NULL
            else:
                p.parameters.asymDetail.symmetric = symmetric
        else:
            for key, value in kwargs.items():
                temp = key.split('.')
                last = temp[-1]
                first = 'p.'+ key.replace(last,'')[:-1]
                setattr(eval(first), last, eval(value))
        if scheme is None:
            p.parameters.asymDetail.scheme.scheme = TPM2_ALG.NULL
        else:
            p.parameters.asymDetail.scheme.scheme = scheme
        if p.type == TPM2_ALG.ECC:
            p.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL
        return p

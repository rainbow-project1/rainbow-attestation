
insens = TPM2B_SENSITIVE_CREATE()
inPublic = TPM2B_PUBLIC()
inPublic.publicArea.type = TPM2_ALG.ECC
# inPublic.publicArea.type = TPM2_ALG.RSA
inPublic.publicArea.nameAlg = TPM2_ALG.SHA256
inPublic.publicArea.authPolicy = Pd

inPublic.publicArea.objectAttributes = (
	TPMA_OBJECT.NODA |
	TPMA_OBJECT.SENSITIVEDATAORIGIN |
	TPMA_OBJECT.USERWITHAUTH |
	TPMA_OBJECT.SIGN_ENCRYPT |
	TPMA_OBJECT.ADMINWITHPOLICY
)

inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
inPublic.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
inPublic.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
inPublic.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

KH_AK, AK_pub, _, _, _ = ectx.CreatePrimary(insens, inPublic)

Hp = hashlib.sha256(finalPolicy).digest()

Hcp = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(name_final_rev)+bytes(name_final_rev) + bytearray(8)).digest()

Ho = hashlib.sha256(b"\x00\x00\x00\x00" + Hcp).digest()

sym = TPMT_SYM_DEF(
		algorithm=TPM2_ALG.XOR,
		keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
		mode=TPMU_SYM_MODE(None),
	)

policySession = ectx.start_auth_session(
		ESYS_TR.NONE,
		ESYS_TR.NONE,
		None,
		TPM2_SE.POLICY,
		sym,
		TPM2_ALG.SHA256,
	)

timeout, policyTicket = ectx.policy_secret(ACI_handle, policySession, b"", b"", b"", 0)

scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

Sp = ectx.sign(KH_AK, Hp, scheme, validation, session1 = policySession)

ectx.FlushContext(policySession)

sym = TPMT_SYM_DEF(
		algorithm=TPM2_ALG.XOR,
		keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
		mode=TPMU_SYM_MODE(None),
	)

policySession = ectx.start_auth_session(
		ESYS_TR.NONE,
		ESYS_TR.NONE,
		None,
		TPM2_SE.POLICY,
		sym,
		TPM2_ALG.SHA256,
	)

timeout, policyTicket = ectx.policy_secret(ACI_handle, policySession, b"", b"", b"", 0)

scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

So = ectx.sign(KH_AK, Ho, scheme, validation, session1 = policySession)

ectx.FlushContext(policySession)

t_verified = ectx.verify_signature(KH_AK, Hp, Sp)

sym = TPMT_SYM_DEF(
		algorithm=TPM2_ALG.XOR,
		keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
		mode=TPMU_SYM_MODE(None),
	)

policySession = ectx.start_auth_session(
		ESYS_TR.NONE,
		ESYS_TR.NONE,
		None,
		TPM2_SE.POLICY,
		sym,
		TPM2_ALG.SHA256,
	)

timeout, policy_ticket = ectx.policy_signed(
		KH_AK, policySession, b"", Hcp, b"", 0, So
	)

ectx.policy_or(policySession, Beta)

AKname = AK_pub.getName()

ectx.PolicyAuthorize(policySession, finalPolicy, b"", AKname, t_verified)

ectx.FlushContext(KH_AK)

ectx.NV_SetBits(Revocation_handle, 0, Revocation_handle, session1 = policySession)

ectx.FlushContext(policySession)
from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import random
import templates as tp

cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2327,host=172.17.0.1")

with ESAPI(tcti) as ectx:
	ectx.startup(TPM2_SU.CLEAR)

	root_handle, root_public, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_ra_root_templ(), primaryHandle = ESYS_TR.NULL)

	sign_priv, sign_public, _, _, _ = ectx.create(root_handle, TPM2B_SENSITIVE_CREATE(), tp.get_ra_pk_templ())

	sign_key_handle = ectx.load(root_handle, sign_priv, sign_public)

	print(f"The sign key handle is at: {hex(sign_key_handle)}")

	signing_key_name = sign_public.getName()

	# print(bytes(signing_key_name))

	with open("ra_pk_name", "wb") as file:
		file.write(bytes(signing_key_name))

	# with open("ra.pem", "wb") as file:
	# 	file.write(sign_public.toPEM())

	ectx.FlushContext(root_handle)

	input("Waiting for box to send us the key's names...")

	print("Now we are registering the cpHashes of the box keys...")
		
	# TODO: Socket
	with open("psk_names", "rb") as file:
		ps1_name = pickle.load(file)
	# End Socket

	print(ps1_name, ps2_name)
##############################initializeRevocationIndex(0x01001011, 0x01010101, "Donne")################################

#########################ASK THANASSIS if reboot is needed#########################

#ectx.shutdown()

#ectx.startup(TPM2_SU.CLEAR)

# from ctypes import cdll
# from tpm2_pytss import *
# from tpm2_pytss.types import *
# import hashlib
# import random

# cdll.LoadLibrary("libtss2-tcti-mssim.so")

# tcti = TctiLdr("mssim", f"port=2321,host=172.17.0.1")

# with ESAPI(tcti) as ectx:

aci_public, aci_name = ectx.NV_ReadPublic(ACI_handle)

Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x51"+bytes(aci_name)).digest()).digest()

insens2 = TPM2B_SENSITIVE_CREATE()
inPublic2 = TPM2B_PUBLIC()
inPublic2.publicArea.authPolicy = Pd
inPublic2.publicArea.type = TPM2_ALG.ECC
# inPublic.publicArea.type = TPM2_ALG.RSA
inPublic2.publicArea.nameAlg = TPM2_ALG.SHA256

inPublic2.publicArea.objectAttributes = (
	TPMA_OBJECT.NODA |
	TPMA_OBJECT.SENSITIVEDATAORIGIN |
	TPMA_OBJECT.USERWITHAUTH |
	TPMA_OBJECT.SIGN_ENCRYPT |
	TPMA_OBJECT.ADMINWITHPOLICY
)

inPublic2.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
inPublic2.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
inPublic2.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
inPublic2.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
inPublic2.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
inPublic2.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

KH_AK, AK_pub, _, _, _ = ectx.CreatePrimary(insens2, inPublic2)

Rev_Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x6a"+bytes(AK_pub.getName())).digest()).digest()

ectx.FlushContext(KH_AK)

revIndex_pub = TPM2B_NV_PUBLIC(
	nvPublic=TPMS_NV_PUBLIC(
		nvIndex=0x01001011,
		nameAlg=TPM2_ALG.SHA256,
		attributes=TPMA_NV.POLICYWRITE | TPMA_NV.OWNERREAD | TPMA_NV.NO_DA | (TPM2_NT.BITS << TPMA_NV.TPM2_NT_SHIFT),
		authPolicy=Rev_Pd,
		dataSize=8,
		)
	)

Revocation_handle = ectx.NV_DefineSpace(b"", revIndex_pub)
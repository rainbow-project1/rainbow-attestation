
public_final_rev, name_final_rev = ectx.NV_ReadPublic(Revocation_handle)

# musth check if this is needed
# public_final_rev.nvPublic.attributes |= TPMA_NV.WRITTEN 

nvNameWritten = public_final_rev.getName()

# the ra_pk_name must come from RA entity
ra_pk_name = b'\x00\x0b>D\xcd\xb8\xe8\xfc\xcc\x91\xa0=\xb9;\x92\n7\x8e\xb3\xce\xd2Dq\x8d\xa8~\xb3\xf4\x13tw)Zc'

bits = ((0, 63), (1,63))

Beta = []

# for more revocation indexes we propably need an anonymizer counter

b0 = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(AK_pub.getName())).digest()).digest()

Beta.append(b0)

for i in range(2):

	l1, l2 = bytearray(32), bytearray(32)

	S_data, H_data = bytearray(8), bytearray(8)

	div, mod = divmod(bits[i][0], 8)

	S_data[7 - div] |= 1 << mod

	div, mod = divmod(bits[i][1], 8)

	H_data[7 - div] |= 1 << mod

	Hs = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(nvNameWritten)+bytes(nvNameWritten) + S_data).digest()

	Hh = hashlib.sha256(b"\x00\x00\x01\x35"+bytes(nvNameWritten)+bytes(nvNameWritten) + H_data).digest() 

	l1 = hashlib.sha256(hashlib.sha256(l1+b"\x00\x00\x01\x60"+bytes(ra_pk_name)).digest()).digest()

	l1 = hashlib.sha256(l1+b"\x00\x00\x01\x6e"+Hs).digest()

	l2 = hashlib.sha256(hashlib.sha256(l2+b"\x00\x00\x01\x60"+bytes(ra_pk_name)).digest()).digest()

	l2 = hashlib.sha256(l2+b"\x00\x00\x01\x6e"+Hh).digest()

	b = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71" + l1 + l2).digest()

	Beta.append(b)

finalPolicy = hashlib.sha256(bytearray(32) + b"\x00\x00\x01\x71"+ b''.join(Beta)).digest()
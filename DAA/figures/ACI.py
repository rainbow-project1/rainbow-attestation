from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import random

cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2321,host=172.17.0.1")

with ESAPI(tcti) as ectx:
	ectx.startup(TPM2_SU.CLEAR)

	insens = TPM2B_SENSITIVE_CREATE()
	inPublic = TPM2B_PUBLIC()
	inPublic.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inPublic.publicArea.nameAlg = TPM2_ALG.SHA256

	inPublic.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inPublic.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inPublic.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inPublic.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inPublic.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inPublic.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	KH_EpK, EpK, _, _, _ = ectx.CreatePrimary(insens, inPublic, primaryHandle = ESYS_TR.NULL)

	Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x60"+bytes(EpK.getName())).digest()).digest()

	secret_value = b"Donne"

	nvpub = TPM2B_NV_PUBLIC(
		nvPublic=TPMS_NV_PUBLIC(
			nvIndex=0x01010101,
			nameAlg=TPM2_ALG.SHA256,
			attributes= TPMA_NV.NO_DA | (TPM2_NT.PIN_PASS << TPMA_NV.TPM2_NT_SHIFT) | TPMA_NV.POLICYWRITE | TPMA_NV.POLICYREAD ,
			authPolicy=Pd,
			dataSize=8,
			)
		)

	ACI_handle = ectx.NV_DefineSpace(secret_value, nvpub)

	hashed = hashlib.sha256(b"\x00\x00\x00\x00").digest()

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
	scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

	authSignature = ectx.sign(KH_EpK, hashed, scheme, validation)

	sym = TPMT_SYM_DEF(
			algorithm=TPM2_ALG.XOR,
			keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
			mode=TPMU_SYM_MODE(None),
		)

	policySession = ectx.start_auth_session(
			ESYS_TR.NONE,
			ESYS_TR.NONE,
			None,
			TPM2_SE.POLICY,
			sym,
			TPM2_ALG.SHA256,
		)

	timeout, policy_ticket = ectx.policy_signed(
			KH_EpK, policySession, b"", b"", b"", 0, authSignature
		)

	data = b'\x00\x00\x00\x00\x00\x00\x00\x02'
	ectx.NV_Write(ACI_handle, data, offset = 0, authHandle = ACI_handle, session1 = policySession)

	# value = ectx.NV_Read(ACI_handle, 8, 0, authHandle = ACI_handle)

	ectx.FlushContext(policySession)
	ectx.FlushContext(KH_EpK)

	exec(open('Init_Rev_Index.py').read())

	# ectx.shutdown(TPM2_SU.STATE)
	# ectx.startup(TPM2_SU.CLEAR)
	# ask Thanassis if the above is a proper reboot

	# value = ectx.NV_Read(ACI_handle, 8, 0, authHandle = ACI_handle)
	# print(value)

	exec(open('Gen_FinalPolDigest.py').read())

	exec(open('Activate_Rev_Index.py').read())

	exec(open('DAA_Revocation.py').read())
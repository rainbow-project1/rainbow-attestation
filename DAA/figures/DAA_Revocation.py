
with open("ra.pem", "rb") as file:
		RAK_pub = file.read()

RAK_pub = TPM2B_PUBLIC.fromPEM(RAK_pub, objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	))

RAK_pub.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
RAK_pub.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
RAK_pub.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
RAK_pub.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
RAK_pub.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
RAK_pub.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

KH_ra = ectx.load_external(None, RAK_pub)

sym = TPMT_SYM_DEF(
		algorithm=TPM2_ALG.XOR,
		keyBits=TPMU_SYM_KEY_BITS(exclusiveOr=TPM2_ALG.SHA256),
		mode=TPMU_SYM_MODE(None),
	)

session = ectx.start_auth_session(
		ESYS_TR.NONE,
		ESYS_TR.NONE,
		None,
		TPM2_SE.POLICY,
		sym,
		TPM2_ALG.SHA256,
	)

# Work has to be done first

# timeout, policy_ticket = ectx.policy_signed(
# 		RAK_pub, session, b"", temp_cpHash, b"", 0, temp_signature
# 	)
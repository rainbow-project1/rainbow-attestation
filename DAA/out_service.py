from flask import Flask, request, abort, jsonify, render_template, url_for, redirect, flash, session
from flask import send_from_directory
import socket
import pickle
import jsonpickle
import os

app = Flask(__name__)

from flask_cors import CORS
CORS(app)
cors = CORS(app, resources={r"/getCarLogs": {"origins": "http://192.168.1.5:3000"}})

file_path = os.path.abspath(__file__)
dirname_path = os.path.dirname(file_path)
UPLOAD_FOLDER_CAR_LOGS = dirname_path

print(file_path)
print(dirname_path)

carID = {"IHJ-8375":"car1testid","EDA-6881":"car2testid"}

def box1(data):
	try:
		sd1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		HOST1, PORT1 = "localhost", 9998

		sd1.connect((HOST1, PORT1))

		data = pickle.dumps(data)

		sd1.sendall(data)

		data = sd1.recv(18364)

		return data

	except:
		sd1.close()

def box2(data):
	try:
		sd_box2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		HOST2, PORT2 = "localhost", 9991

		sd_box2.connect((HOST2, PORT2))

		data = pickle.dumps(data)

		sd_box2.sendall(data)

		data = sd_box2.recv(18364)

		sd_box2.close()

		return data

	except:
		sd_box2.close()

def RA(data):
	try:
		sd1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		HOST1, PORT1 = "localhost", 9995

		sd1.connect((HOST1, PORT1))

		data = pickle.dumps(data)

		sd1.sendall(data)

		data = sd1.recv(4096)

		return data

	except:
		sd1.close()

@app.route('/api/v1/vehicle/start_join', methods=['POST'])
def start():
	car_id = request.get_json().get("carID")
	data = ["start"]
	if str(car_id) =="car1testid":
		returned = box1(data)
	elif str(car_id) == "car2testid":
		returned = box2(data)
	print (pickle.loads(returned))
	encodedPickle = jsonpickle.encode(returned)
	#verified = pickle.loads(returned)
	#return verified[0]
	return encodedPickle


@app.route('/api/v1/vehicle/activate_keys', methods=['POST'])
def activation():
	car_id = request.get_json().get("carID")
	data = ["activate"]
	if str(car_id) =="car1testid":
		activated = box1(data)
	elif str(car_id) == "car2testid":
		activated = box2(data)
	print (pickle.loads(activated))
	encodedActivatedPickle = jsonpickle.encode(activated)
	#activated = pickle.loads(activated)
	return encodedActivatedPickle
	#return str(activated)

@app.route('/api/v1/vehicle/location_msg', methods=['POST'])
def push():
	car_id = request.get_json().get("carID")
	msg = request.get_json().get("message")
	Sigtype = request.get_json().get("type")
	data = ["push", msg, Sigtype]
	if str(car_id) =="car1testid":
		status = box1(data)
	elif str(car_id) == "car2testid":
		status = box2(data)
	#status = pickle.loads(status)
	print (pickle.loads(status))
	encodedMessage = jsonpickle.encode(status)
	return encodedMessage

from urllib.parse import unquote

@app.route('/api/v1/vehicle/revoke', methods=['POST'])
def verification():

	pseudonym = request.get_json().get("pseudonym")
	Rtype = request.get_json().get("Rtype")
	###Remove for Entzo
	# pseudonym = unquote(pseudonym)
	# print(pseudonym)
	###
	data = ["revoke", pseudonym, Rtype]
	status = RA(data)
	print (pickle.loads(status))
	encodedRevoke = jsonpickle.encode(status)

	return encodedRevoke


@app.route('/api/v1/vehicle/reboot', methods=['POST'])
def reboot():
	car_id = request.get_json().get("carID")
	data = ["reboot"]
	if str(car_id) == "car1testid":
		returned = box1(data)
	elif str(car_id) == "car2testid":
		returned = box2(data)
	print(pickle.loads(returned))
	encodedPickle = jsonpickle.encode(returned)
	return encodedPickle


@app.route('/getCarLogs', methods=['POST'])
def carLogs():
  carNameFront = request.get_json().get("carName")
  logFile = str(carID[carNameFront]) + ".log"
  return send_from_directory(UPLOAD_FOLDER_CAR_LOGS, logFile, as_attachment=True)


if __name__ == '__main__':
	app.run(host="0.0.0.0")

from tpm2_pytss.types import *

def get_endorsement_key_templ():

	inP = TPM2B_PUBLIC()
	inP.publicArea.type = TPM2_ALG.RSA
	inP.publicArea.nameAlg = TPM2_ALG.SHA256
	inP.publicArea.objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.RESTRICTED |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.DECRYPT
	)

	authPolicy = bytes((
	0x83, 0x71, 0x97, 0x67, 0x44, 0x84, 0xB3, 0xF8, 0x1A, 0x90, 0xCC,
	0x8D, 0x46, 0xA5, 0xD7, 0x24, 0xFD, 0x52, 0xD7, 0x6E, 0x06, 0x52,
	0x0B, 0x64, 0xF2, 0xA1, 0xDA, 0x1B, 0x33, 0x14, 0x69, 0xAA ))

	inP.publicArea.authPolicy = authPolicy

	inP.publicArea.parameters.rsaDetail.symmetric.algorithm = TPM2_ALG.AES
	inP.publicArea.parameters.rsaDetail.symmetric.keyBits.aes = 128
	inP.publicArea.parameters.rsaDetail.symmetric.mode.aes=TPM2_ALG.CFB
	inP.publicArea.parameters.rsaDetail.scheme.scheme = TPM2_ALG.NULL
	inP.publicArea.parameters.rsaDetail.keyBits = 2048
	inP.publicArea.parameters.rsaDetail.exponent = 0

	return inP

def get_ephemeral_key_templ():

	inP = TPM2B_PUBLIC()
	inP.publicArea.type = TPM2_ALG.ECC
	# inP.publicArea.type = TPM2_ALG.RSA
	inP.publicArea.nameAlg = TPM2_ALG.SHA256

	inP.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inP.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inP.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inP.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inP.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inP.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inP.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	return inP


def get_daa_key_templ():

	inP = TPM2B_PUBLIC()
	inP.publicArea.type = TPM2_ALG.ECC
	inP.publicArea.nameAlg = TPM2_ALG.SHA256

	inP.publicArea.objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.RESTRICTED |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT
	)

	inP.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL
	inP.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDAA
	inP.publicArea.parameters.eccDetail.scheme.details.ecdaa.hashAlg = TPM2_ALG.SHA256
	inP.publicArea.parameters.eccDetail.scheme.details.ecdaa.count = 1
	inP.publicArea.parameters.eccDetail.curveID = TPM2_ECC.BN_P256
	inP.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL

	return inP


def get_ps_key_templ(policy = None):

	inP = TPM2B_PUBLIC()
	inP.publicArea.type = TPM2_ALG.ECC
	inP.publicArea.nameAlg = TPM2_ALG.SHA256
	inP.publicArea.objectAttributes = (
		# TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.NODA |
		# TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.ADMINWITHPOLICY |
		TPMA_OBJECT.SIGN_ENCRYPT
	)

	if (policy != None):
		inP.publicArea.authPolicy = policy 
	inP.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL
	inP.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA
	inP.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
	inP.publicArea.parameters.eccDetail.curveID = TPM2_ECC.BN_P256
	inP.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL

	return inP

def get_ra_root_templ():

	inP = TPM2B_PUBLIC()
	inP.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inP.publicArea.nameAlg = TPM2_ALG.SHA256

	inP.publicArea.objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.DECRYPT |
		TPMA_OBJECT.RESTRICTED
	)

	inP.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.AES;
	inP.publicArea.parameters.eccDetail.symmetric.keyBits.aes = 128;
	inP.publicArea.parameters.eccDetail.symmetric.mode.aes = TPM2_ALG.CFB;
	inP.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.NULL;
	inP.publicArea.parameters.eccDetail.scheme.details.anySig.hashAlg = 0;
	inP.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inP.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inP.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = 0;

	return inP

def get_ra_pk_templ():

	inP = TPM2B_PUBLIC()
	inP.publicArea.type = TPM2_ALG.ECC
	# inPublic.publicArea.type = TPM2_ALG.RSA
	inP.publicArea.nameAlg = TPM2_ALG.SHA256

	inP.publicArea.objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	)

	inP.publicArea.parameters.eccDetail.symmetric.algorithm = TPM2_ALG.NULL;
	inP.publicArea.parameters.eccDetail.scheme.scheme = TPM2_ALG.ECDSA;
	inP.publicArea.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256;
	inP.publicArea.parameters.eccDetail.curveID = TPM2_ECC.NIST_P256;
	inP.publicArea.parameters.eccDetail.kdf.scheme = TPM2_ALG.NULL;
	inP.publicArea.parameters.eccDetail.kdf.details.mgf1.hashAlg = TPM2_ALG.SHA256;

	return inP

def load_ek_pem(ek_public):

	d = {"parameters.rsaDetail.symmetric.algorithm": 'TPM2_ALG.AES', "parameters.rsaDetail.symmetric.keyBits.aes": '128', "parameters.rsaDetail.symmetric.mode.aes": 'TPM2_ALG.CFB', \
			"parameters.rsaDetail.scheme.scheme": 'TPM2_ALG.NULL', "parameters.rsaDetail.keyBits": '2048', "parameters.rsaDetail.exponent": '0'}

	return TPM2B_PUBLIC.fromPEM(ek_public, objectAttributes = (
		TPMA_OBJECT.FIXEDTPM |
		TPMA_OBJECT.FIXEDPARENT |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.RESTRICTED |
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.DECRYPT
	), **d)


def load_sign_ra_pem(sign_public):

	d = {"parameters.eccDetail.symmetric.algorithm": 'TPM2_ALG.NULL', "parameters.eccDetail.scheme.scheme": 'TPM2_ALG.ECDSA', \
			 "parameters.eccDetail.scheme.details.ecdsa.hashAlg": 'TPM2_ALG.SHA256', "parameters.eccDetail.curveID": 'TPM2_ECC.NIST_P256', \
			 "parameters.eccDetail.kdf.scheme": 'TPM2_ALG.NULL' , "parameters.eccDetail.kdf.details.mgf1.hashAlg": 'TPM2_ALG.SHA256'}

	return TPM2B_PUBLIC.fromPEM(sign_public, objectAttributes = (
		TPMA_OBJECT.NODA |
		TPMA_OBJECT.SENSITIVEDATAORIGIN |
		TPMA_OBJECT.USERWITHAUTH |
		TPMA_OBJECT.SIGN_ENCRYPT |
		TPMA_OBJECT.ADMINWITHPOLICY
	), **d)

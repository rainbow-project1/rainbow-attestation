import ctypes
import hashlib
import utils
import sys
from Crypto.Random import get_random_bytes

ssl_library = ctypes.cdll.LoadLibrary("libssl.so")
# ssl_library = ctypes.cdll.dlopen("/usr/lib/x86_64-linux-gnu/libssl.so")
# ssl = ctypes.pythonapi()
# ssl_library = ctypes.PyDLL("/usr/lib/x86_64-linux-gnu/libssl.so")
# int_ssl._handle = int_ssl.dlopen("/usr/lib/x86_64-linux-gnu/libssl.so")
# ssl_library = ssl_library.dlopen(ssl_library._name, ctypes.RTLD_LOCAL)
# ssl_library = sys.dlopen(LM_ID_NEWLM, "mylib.so", RTLD_LAZY | RTLD_DEEPBIND)
# ssl_library = ctypes.PyDLL("/usr/lib/x86_64-linux-gnu/libssl.so")



def BN_num_bytes(a):

	return ((ssl_library.BN_num_bits(a)+7) // 8)

def point2bb(curve, p):

	ctx = ssl_library.BN_CTX_new()
	x_bn = ssl_library.BN_new()
	y_bn = ssl_library.BN_new()

	res = ssl_library.EC_POINT_get_affine_coordinates_GFp(curve , p, x_bn, y_bn, ctx)
	if (res != 1):
		raise Exception("point2bb failed")

	sz1 = BN_num_bytes(x_bn)

	bb = ctypes.create_string_buffer(sz1)

	ssl_library.BN_bn2bin(x_bn, bb)

	sz2 = BN_num_bytes(y_bn)

	bb2 = ctypes.create_string_buffer(sz2)

	ssl_library.BN_bn2bin(y_bn, bb2)

	ssl_library.BN_free(x_bn)
	ssl_library.BN_free(y_bn)
	ssl_library.BN_CTX_free(ctx)

	return (bb.raw, bb2.raw)


def ec_generator_mul(curve, z):
	ctx = ssl_library.BN_CTX_new()
	m_bn = ssl_library.BN_new()

	ssl_library.BN_bin2bn(z, len(z), m_bn)

	res = ssl_library.EC_POINT_new(curve)

	result = ssl_library.EC_POINT_mul(curve, res, m_bn, None, None, ctx)

	ssl_library.BN_CTX_free(ctx)
	ssl_library.BN_free(m_bn)

	if (result != 1):
		raise SystemExit("ec_generator_mul_failed")

	return point2bb(curve, res)


def point_is_on_curve(curve, pt_bb):
	ctx = ssl_library.BN_CTX_new()
	pt = ssl_library.EC_POINT_new(curve)

	bb2point(curve, pt_bb, pt)

	result = ssl_library.EC_POINT_is_on_curve(curve, pt, ctx)
	if (result!=1):
		print("Point is not on curve")

	ssl_library.BN_CTX_free(ctx)
	ssl_library.EC_POINT_free(pt)

	return result



def bb2point(curve, pt_bb:tuple, pt):

	ctx2 = ssl_library.BN_CTX_new()
	x_bn = ssl_library.BN_new()
	y_bn = ssl_library.BN_new()

	pt_bb0 = ctypes.create_string_buffer(pt_bb[0])
	pt_bb1 = ctypes.create_string_buffer(pt_bb[1])


	ssl_library.BN_bin2bn(pt_bb0, len(pt_bb0)-1, x_bn)
	ssl_library.BN_bin2bn(pt_bb1, len(pt_bb1)-1, y_bn)

	result = ssl_library.EC_POINT_set_affine_coordinates_GFp(curve, pt, x_bn, y_bn, ctx2)

	ssl_library.BN_free(x_bn)
	ssl_library.BN_free(y_bn)
	ssl_library.BN_CTX_free(ctx2)

	if (result != 1):
		print("bb2point failed")
		# raise Exception("bb2point failed")


def ec_point_mul(curve, z, pt_bb:tuple):
	ctx = ssl_library.BN_CTX_new()
	m_bn = ssl_library.BN_new()

	ssl_library.BN_bin2bn(z, len(z), m_bn)

	pt = ssl_library.EC_POINT_new(curve)

	bb2point(curve, pt_bb, pt)

	res = ssl_library.EC_POINT_new(curve)

	result = ssl_library.EC_POINT_mul(curve, res, None, pt, m_bn, ctx)

	ssl_library.BN_CTX_free(ctx)
	ssl_library.BN_free(m_bn)
	ssl_library.EC_POINT_free(pt)

	if (result != 1):
		raise SystemExit("ec_point_mul_failed")

	return point2bb(curve, res)



def ec_point_invert(curve, pt_bb:tuple):

	ctx = ssl_library.BN_CTX_new()

	pt = ssl_library.EC_POINT_new(curve)

	bb2point(curve, pt_bb, pt)

	result = ssl_library.EC_POINT_invert(curve, pt, ctx)

	ssl_library.BN_CTX_free(ctx)

	if (result != 1):
		raise SystemExit("ec_point_mul_failed")

	return point2bb(curve, pt)



def ec_point_add(curve, pt_a_bb:tuple, pt_b_bb:tuple):

	ctx = ssl_library.BN_CTX_new()
	# ssl_library.BN_bin2bn(z, len(z), m_bn)

	pt_a = ssl_library.EC_POINT_new(curve)

	bb2point(curve, pt_a_bb, pt_a)

	pt_b = ssl_library.EC_POINT_new(curve)

	bb2point(curve, pt_b_bb, pt_b)

	pt_r = ssl_library.EC_POINT_new(curve)

	result = ssl_library.EC_POINT_add(curve, pt_r, pt_a, pt_b, ctx)

	ssl_library.BN_CTX_free(ctx)
	ssl_library.EC_POINT_free(pt_b)
	ssl_library.EC_POINT_free(pt_a)


	if (result != 1):
		raise SystemExit("ec_point_add_failed")

	return point2bb(curve, pt_r)

def get_BNP256_curve():
	bnp256_p = bytes.fromhex("FFFFFFFFFFFCF0CD46E5F25EEE71A49F0CDC65FB12980A82D3292DDBAED33013")
	bnp256_p_buff = ctypes.create_string_buffer(bnp256_p)

	bnp256_a_buff = ctypes.create_string_buffer(b"\x00")

	bnp256_b_buff = ctypes.create_string_buffer(b"\x03")

	tmp_1 = ssl_library.BN_bin2bn(bnp256_p_buff, len(bnp256_p_buff)-1, None)
	tmp_2 = ssl_library.BN_bin2bn(bnp256_a_buff, len(bnp256_a_buff)-1, None)
	tmp_3 = ssl_library.BN_bin2bn(bnp256_b_buff, len(bnp256_b_buff)-1, None)

	curve = ssl_library.EC_GROUP_new_curve_GFp(tmp_1, tmp_2, tmp_3, None)

	ctx = ssl_library.BN_CTX_new()

	generator = ssl_library.EC_POINT_new(curve);

	bnp256_order = bytes.fromhex("FFFFFFFFFFFCF0CD46E5F25EEE71A49E0CDC65FB1299921AF62D536CD10B500D")
	bnp256_order_buff = ctypes.create_string_buffer(bnp256_order)

	bnp256_gX_buff = ctypes.create_string_buffer(b"\x01")

	bnp256_gY_buff = ctypes.create_string_buffer(b"\x02")

	tmp_1 = ssl_library.BN_bin2bn(bnp256_gX_buff, len(bnp256_gX_buff)-1, tmp_1)
	tmp_2 = ssl_library.BN_bin2bn(bnp256_gY_buff, len(bnp256_gY_buff)-1, tmp_2)

	if (ssl_library.EC_POINT_set_affine_coordinates_GFp(curve,generator,tmp_1,tmp_2,ctx) != 1):
		raise Exception("Set affine coordinates failed")

	tmp_1 = ssl_library.BN_bin2bn(bnp256_order_buff, len(bnp256_order_buff)-1, tmp_1)

	if (ssl_library.EC_GROUP_set_generator(curve,generator,tmp_1,tmp_2) != 1):
		raise Exception("Set generator coordinates failed")

	ssl_library.BN_free(tmp_1)
	ssl_library.BN_free(tmp_2)
	ssl_library.BN_free(tmp_3)
	ssl_library.EC_POINT_free(generator)
	ssl_library.BN_CTX_free(ctx)

	return curve

def check_curve(curve):
	ctx = ssl_library.BN_CTX_new()

	rc = ssl_library.EC_GROUP_check(curve,ctx)
	if ( rc != 1):
		raise Exception("ec group check failed")

	ssl_library.BN_CTX_free(ctx)


def host_points_creation(curve, cre, daa_public_key):

	r_cre1 = []

	temp = get_random_bytes(32)
	l = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	for i in cre:
		r_cre1.append(ec_point_mul(curve, l, i))

	res = point_is_on_curve(curve, r_cre1[1])

	if (res != 1):
		raise Exception("S is not on the curve")


	bsn = hashlib.sha256(b"".join(daa_public_key)).digest()

	ctx = ssl_library.BN_CTX_new()
	p_bn = ssl_library.BN_new()
	a_bn = ssl_library.BN_new()
	b_bn = ssl_library.BN_new()

	res = ssl_library.EC_GROUP_get_curve_GFp(curve, p_bn, a_bn, b_bn, ctx)
	if (res != 1):
		raise Exception("Failed to get the curve parameters")

	p_bb = ctypes.create_string_buffer(BN_num_bytes(p_bn))

	ssl_library.BN_bn2bin(p_bn, p_bb)

	max_map_tries = 10
	point_is_on_curve2 = 0
	counter = 1

	x_bn = ssl_library.BN_new()
	y_bn = ssl_library.BN_new()
	xsq_bn = ssl_library.BN_new()
	ysq_bn = ssl_library.BN_new()

	while (point_is_on_curve2 != 1 and counter < max_map_tries):

		bb = bytearray(4)
		ival = counter

		for i in range(4):
			bb[3-i]=ival&0xff
			ival >>=8

		s_2 = bb + bsn

		temp = hashlib.sha256(s_2).hexdigest()
		a = int(temp, 16)
		x_2 = utils.int_to_bytes(a % int.from_bytes(p_bb.raw, "big"))

		x_2 = ctypes.create_string_buffer(x_2)

		ssl_library.BN_bin2bn(x_2, len(x_2)-1, x_bn)

		ssl_library.BN_mod_sqr(xsq_bn, x_bn, p_bn, ctx)
		ssl_library.BN_mod_add(ysq_bn, x_bn, a_bn, p_bn, ctx)
		ssl_library.BN_mod_mul(ysq_bn, ysq_bn, xsq_bn, p_bn, ctx)
		ssl_library.BN_mod_add(ysq_bn, ysq_bn, b_bn, p_bn, ctx)
		ssl_library.BN_mod_sqrt(y_bn, ysq_bn, p_bn, ctx)

		y_2 = ctypes.create_string_buffer(BN_num_bytes(y_bn))
		ssl_library.BN_bn2bin(y_bn, y_2)

		pt = (x_2.raw[:-1], y_2.raw)

		point_is_on_curve2 = point_is_on_curve(curve, pt)

		counter += 1

	ssl_library.BN_free(x_bn)
	ssl_library.BN_free(xsq_bn)
	ssl_library.BN_free(y_bn)
	ssl_library.BN_free(ysq_bn)

	ssl_library.BN_free(p_bn)
	ssl_library.BN_free(a_bn)
	ssl_library.BN_free(b_bn)
	ssl_library.BN_CTX_free(ctx)

	map_pt = (bytes(s_2), y_2.raw)

	temp = hashlib.sha256(map_pt[0]).hexdigest()
	temp2 = utils.int_to_bytes(int(temp, 16) % utils.BNP256_P)

	pt_j = (temp2, map_pt[1])

	if (point_is_on_curve(curve, pt_j) != 1):
		raise Exception("J is not on the curve")

	return (r_cre1, map_pt, pt_j)



def psk_sign_verification(curve, pt_psk, Scm, digest_to_sign):

	ec_key = ssl_library.EC_KEY_new()

	ssl_library.EC_KEY_set_group(ec_key, curve)

	pub_key = ssl_library.EC_POINT_new(curve)

	bb2point(curve, pt_psk, pub_key)

	if (ssl_library.EC_KEY_set_public_key(ec_key, pub_key) != 1):
		raise Exception("Failed to load public key (OpenSSL)")

	sig_s = Scm[1]
	sig_r = Scm[2]

	r = ssl_library.BN_new()

	ssl_library.BN_bin2bn(sig_r, len(sig_r), r)

	s = ssl_library.BN_new()

	ssl_library.BN_bin2bn(sig_s, len(sig_s), s)

	ossl_sig= ssl_library.ECDSA_SIG_new()

	ssl_library.ECDSA_SIG_set0(ossl_sig, r, s)

	res = ssl_library.ECDSA_do_verify(digest_to_sign, len(digest_to_sign), ossl_sig, ec_key)

	if (res != 1):
		print("Signature verification Failed")
		res = False
	else:
		print("Signature verification OK")
		res = True

	ssl_library.ECDSA_SIG_free(ossl_sig)
	ssl_library.EC_POINT_free(pub_key)
	ssl_library.EC_KEY_free(ec_key)

	return res


def psk_DAA_verify(curve, Scm, message, Points):

	sig_s = Scm[1]
	hash2 = Scm[0]

	message = message.encode()

	s_j_bb = ec_point_mul(curve, sig_s, Points[4])
	res = point_is_on_curve(curve, s_j_bb)

	if (res != 1):
		raise Exception("[s]J is not on the curve")

	h2_k_bb = ec_point_mul(curve, hash2, Points[5])
	res = point_is_on_curve(curve, h2_k_bb)

	if (res != 1):
		raise Exception("[h_2]K is not on the curve")

	tpm_bb = ec_point_invert(curve, h2_k_bb)
	l_prime_bb = ec_point_add(curve, s_j_bb, tpm_bb)
	res = point_is_on_curve(curve, l_prime_bb)

	if (res != 1):
		raise Exception("L is not on the curve")

	s_pt_s_bb = ec_point_mul(curve, sig_s, Points[1])
	res = point_is_on_curve(curve, s_pt_s_bb)

	if (res != 1):
		raise Exception("[s]S is not on the curve")

	h2_pt_w_bb = ec_point_mul(curve, hash2, Points[3])
	res = point_is_on_curve(curve, h2_pt_w_bb)

	if (res != 1):
		raise Exception("[h_2]W is not on the curve")

	tpm_bb = ec_point_invert(curve, h2_pt_w_bb)
	e_prime_bb = ec_point_add(curve, s_pt_s_bb, tpm_bb)
	res = point_is_on_curve(curve, e_prime_bb)

	if (res != 1):
		raise Exception("E is not on the curve")

	md_ = hashlib.sha256(message).digest()
	c_ = hashlib.sha256(md_ + b"".join(b"".join(p) for p in Points) + \
								b"".join(l_prime_bb) + b"".join(e_prime_bb)).digest()

	h1_ = hashlib.sha256(c_).digest()

	nm = Scm[2]

	temp_hash = hashlib.sha256(nm + h1_).hexdigest()
	h2_ = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	if (h2_ == hash2):
		print("Signature verification OK")
		return True
	else:
		print("Signature verification Failed")
		return False
	
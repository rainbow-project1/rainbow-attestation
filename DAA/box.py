from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import utils
import templates as tp
import pickle
import utils
import openssl_utils
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import revocation_utils
import communication_utils as coms
import socket
import time

def reboot(ectx):

	if(ectx != None):

		ectx.shutdown(TPM2_SU.STATE)
		print("TPM was successfully shutdowned")
		ectx.startup(TPM2_SU.CLEAR)
		print("TPM REBOOTED successfully")

		return ectx
	else:
		raise Exception("ERROR occured with the ectx context")


def init_join_phase(ek_handle):

	print("Creating daa key...")

	daa_priv, daa_public, _, _, _ = ectx.create(ek_handle, TPM2B_SENSITIVE_CREATE(), tp.get_daa_key_templ())

	daa_handle = ectx.load(ek_handle, daa_priv, daa_public)

	print(f"The daa handle is at: {hex(daa_handle)}") 

	# Socket
	arr = []
	arr.append(ek_public.toPEM())
	arr.append(bytes(daa_public.getName()))
	arr.append(bytes(daa_public.publicArea.unique.ecc.x))
	arr.append(bytes(daa_public.publicArea.unique.ecc.y))

	data = pickle.dumps(arr)
	coms.issuer_client(data)
	# End Socket

	daa_public_key = (bytes(daa_public.publicArea.unique.ecc.x), bytes(daa_public.publicArea.unique.ecc.y))

	print("Waiting for issuer's make_credential")

	# Socket
	c, addr = sd.accept()

	data = c.recv(4096)
	cred, secret = pickle.loads(data)
	c.close()
	# End Socket

	K1 = ectx.activate_credential(daa_handle, ek_handle, cred, secret)

	pk_x, pk_y = utils.get_iso_point()

	string = b"".join(pk_x[0]) + b"".join(pk_x[1]) + b"".join(pk_y[0]) + b"".join(pk_y[1]) + bytes(K1)

	string += bytes(ek_public.publicArea.unique.rsa.buffer)

	_, _, E, counter = ectx.commit(daa_handle, TPM2B_ECC_POINT(), TPM2B_SENSITIVE_DATA(), TPM2B_ECC_PARAMETER())

	E = bytes(E.point.x.buffer) + bytes(E.point.y.buffer)

	print(f"Commit was executed and the values are: {E=} {counter=}")
	
	p = hashlib.sha256(utils.get_bnp_P1() + b"".join(daa_public_key) + E + string).digest()

	p_tpm, validation = ectx.hash(p, TPM2_ALG.SHA256, ESYS_TR.ENDORSEMENT)

	print("tpm2_hash finished!")

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDAA)
	scheme.details.ecdaa.hashAlg = TPM2_ALG.SHA256
	scheme.details.ecdaa.count = counter

	signature = ectx.sign(daa_handle, p_tpm, scheme, validation)
	nj = bytes(signature.signature.ecdaa.signatureR.buffer)
	w = bytes(signature.signature.ecdaa.signatureS.buffer)

	print("tpm_sign finished!")

	temp_hash = hashlib.sha256(nj + bytes(p_tpm)).hexdigest()
	v = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	Sch = [v, w, nj]

	return daa_handle, daa_public_key, daa_priv, daa_public, K1, Sch


def host_verifies_AK_creds(curve, cre, s_cre):

	(A, B, C, D) = cre

	(u, j) = s_cre

	p1 = (b"\x01", b"\x02")

	j_p1_bb = openssl_utils.ec_point_mul(curve, j, p1)
	res = openssl_utils.point_is_on_curve(curve, j_p1_bb)

	if (res != 1):
		raise Exception("[j]P1 is not on the curve")

	u_b_bb = openssl_utils.ec_point_mul(curve, u, B)
	res = openssl_utils.point_is_on_curve(curve, u_b_bb)

	if (res != 1):
		raise Exception("[u]B is not on the curve")

	tpm_bb = openssl_utils.ec_point_invert(curve, u_b_bb)
	Rb_prime_bb = openssl_utils.ec_point_add(curve, j_p1_bb, tpm_bb)
	res = openssl_utils.point_is_on_curve(curve, Rb_prime_bb)

	if (res != 1):
		raise Exception("Rb is not on the curve")

	j_qs_bb = openssl_utils.ec_point_mul(curve, j, daa_public_key)
	rc = openssl_utils.point_is_on_curve(curve, j_qs_bb)

	if (rc != 1):
		raise Exception("[j]Qs is not on curve")

	u_d_bb = openssl_utils.ec_point_mul(curve, u, D)
	rc = openssl_utils.point_is_on_curve(curve, u_d_bb)

	if (rc != 1):
		raise Exception("[u]D is not on curve")
		

	tpm_bb_prime = openssl_utils.ec_point_invert(curve, u_d_bb)

	Rd_prime_bb = openssl_utils.ec_point_add(curve, j_qs_bb, tpm_bb_prime)
	rc = openssl_utils.point_is_on_curve(curve, Rd_prime_bb)

	if (rc != 1):
		raise Exception("Rd is not on curve")

	p1 = utils.get_bnp_P1()

	u_prime = hashlib.sha256(p1 + b"".join(daa_public_key) + b"".join(A) + b"".join(B) + b"".join(C) + b"".join(D) + b"".join(Rb_prime_bb) + b"".join(Rd_prime_bb)).digest()

	if (u == u_prime):
		print("Verified AK Creds OK")
	else:
		raise SystemExit("JOIN failed")


def daa_sign_phase(r_cre1, map_pt, J, daa_handle):

	pt_s = r_cre1[1]

	p1 = TPM2B_ECC_POINT(TPMS_ECC_POINT(x = pt_s[0], y = pt_s[1]))
	s2 = TPM2B_SENSITIVE_DATA(map_pt[0])
	y2 = TPM2B_ECC_PARAMETER(map_pt[1])

	K, L, E, counter = ectx.commit(daa_handle, p1, s2, y2)

	print("tpm2_commit for DAA Sign finished!")

	# R,S,T,W = r_cre1
	Points = r_cre1
	Points.append(J)

	K = (bytes(K.point.x), bytes(K.point.y))
	Points.append(K)
	L = (bytes(L.point.x), bytes(L.point.y))
	Points.append(L)
	E = (bytes(E.point.x), bytes(E.point.y))
	Points.append(E)

	message = b"Speed 70 mph, traffic light"
	md = hashlib.sha256(message).digest()

	c = hashlib.sha256(md + b"".join(b"".join(p) for p in Points)).digest()

	h1, validation = ectx.hash(c, TPM2_ALG.SHA256, ESYS_TR.ENDORSEMENT)

	print("tpm2_hash for DAA Sign finished!")

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDAA)
	scheme.details.ecdaa.hashAlg = TPM2_ALG.SHA256
	scheme.details.ecdaa.count = counter

	signature = ectx.sign(daa_handle, h1, scheme, validation)

	print("tpm2_sign for DAA Sign finished!")

	nm = bytes(signature.signature.ecdaa.signatureR.buffer)
	s = bytes(signature.signature.ecdaa.signatureS.buffer)

	temp_hash = hashlib.sha256(nm + bytes(h1)).hexdigest()
	h2 = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	Scm = [h2, s, nm]

	Points = Points[:-2] # Here we are removing the L and E points cause these must be recreated by issuer for verification

	return Scm, Points, message



def psk_daa_sign(ps_public_key, daa_handle, msg):

	# ps_public_key = (bytes(ps_public.publicArea.unique.ecc.x), bytes(ps_public.publicArea.unique.ecc.y))

	nonce_random_bytes = get_random_bytes(4 * 32)

	temp = nonce_random_bytes[:32]
	r = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	temp = nonce_random_bytes[32:64]
	x = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	temp = nonce_random_bytes[64:96]
	y = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	p1 = (b"\x01", b"\x02")
	cre = []

	curve = openssl_utils.get_BNP256_curve()

	openssl_utils.check_curve(curve)

	cre.append(openssl_utils.ec_point_mul(curve, r, p1)) #point A
	cre.append(openssl_utils.ec_point_mul(curve, y, cre[0])) #point B

	ry = utils.int_to_bytes((int.from_bytes(r, "big") * int.from_bytes(y, "big")) % utils.BNP256_ORDER)

	temp = openssl_utils.ec_point_mul(curve, ry, ps_public_key)

	cre.append(openssl_utils.ec_point_mul(curve, x, temp)) #point C
	cre.append(temp) #point D


	r_cre1, map_pt, J = openssl_utils.host_points_creation(curve, cre, ps_public_key)

	pt_s = r_cre1[1]

	p1 = TPM2B_ECC_POINT(TPMS_ECC_POINT(x = pt_s[0], y = pt_s[1]))
	s2 = TPM2B_SENSITIVE_DATA(map_pt[0])
	y2 = TPM2B_ECC_PARAMETER(map_pt[1])

	K, L, E, counter = ectx.commit(daa_handle, p1, s2, y2)

	print("tpm2_commit for pseud Sign finished!")

	# R,S,T,W = r_cre1
	Points = r_cre1
	Points.append(J)

	K = (bytes(K.point.x), bytes(K.point.y))
	Points.append(K)
	L = (bytes(L.point.x), bytes(L.point.y))
	Points.append(L)
	E = (bytes(E.point.x), bytes(E.point.y))
	Points.append(E)
	
	md = hashlib.sha256(msg).digest()

	c = hashlib.sha256(md + b"".join(b"".join(p) for p in Points)).digest()

	Points = Points[:-2]

	return c, Points




cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2321,host=172.17.0.1")

try:

	sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	print("[*]SOCKET: Socket successfully created")

	HOST, PORT = "localhost", 9998

	sd.bind((HOST, PORT))

	print("[*]SOCKET: Socket binded successfully")

	sd.listen()

	print("[*]SOCKET: Box is listening...")

except:
	sd.close()

	raise SystemExit("[*]SOCKET: Could not establish connection")

with ESAPI(tcti) as ectx:

	ectx.startup(TPM2_SU.CLEAR)

	pseudonyms = 7

	nvCounterIndex = 0x01010101

	prime_timer = time.time()

	data = b"Ready"
	coms.RA_client(data)

	# Here we integrate the revocation initialize phase until we have a revocation handle ready 

	print("Creating authorization counter index...")
	start = time.time()
	ACI_handle = revocation_utils.auth_counter_index(ectx, nvCounterIndex)
	end = time.time()
	print("[*]TIMING: Authorization counter index creation takes", end-start)

	# TODO: Reboot of tpm and check that ACI works the same

	ectx = reboot(ectx)

	print("Creating revocation index...")
	start = time.time()
	Revocation_handle = revocation_utils.init_revocation_index(ectx, ACI_handle)
	end = time.time()
	print("[*]TIMING: Revocation index creation takes", end-start)
	########################################################
	########################################################

	print("Creating endorsement key...")

	ek_handle, ek_public, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_endorsement_key_templ(), primaryHandle=ESYS_TR.ENDORSEMENT)

	print("[*]SOCKET: Loading RA's signing key...")

	# Socket
	c, addr = sd.accept()
	data = c.recv(4096)

	signing_key_public = pickle.loads(data)

	c.close()
	# End Socket

	sign_public = tp.load_sign_ra_pem(signing_key_public)

	signing_key_name = sign_public.getName()

	# For now we assuming 2 pseudonyms
	# bits = [(0,63), (1,63)]
	bits = []
	for i in range(pseudonyms):
		bits.append((i+1, 0))

	#######################################################################
	# We recreate AK handle for using it for activating revocation handle
	#######################################################################

	aci_public, aci_name = ectx.NV_ReadPublic(ACI_handle)

	Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x51"+bytes(aci_name)).digest()).digest()

	inP = tp.get_ephemeral_key_templ()
	inP.publicArea.authPolicy = Pd

	KH_AK, AK_pub, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), inP)

	########################################################
	########################################################

	print("Generating final policy digest...")
	start = time.time()
	finalPolicy, name_final_rev, allDigests, cpHash, policyDigests, subDigests, finalDigests = revocation_utils.get_final_pol_digest(ectx, Revocation_handle, AK_pub, signing_key_name, bits)
	end = time.time()
	print("[*]TIMING: Generating final policy takes", end-start)

	print("Activating revocation index...")
	start = time.time()
	AKname, t_verified = revocation_utils.activate_revoc_index(ectx, finalPolicy, name_final_rev, KH_AK, AK_pub, finalDigests, Revocation_handle, ACI_handle)
	end = time.time()
	print("[*]TIMING: Activating revocation index takes", end-start)

	print("Finished Revocation utils initialization..")

	print("Now we create 2 pseudonym keys...")

	########################################################
	# now we create our pseudonyms!
	########################################################

	# bits = []
	hierarchy = []
	start = time.time()
	for i in range(pseudonyms):

		# bits.append((i, 63))

		policyDigest = revocation_utils.getPolicyNVDigest(ectx, bits[i][0], bits[i][1], Revocation_handle)

		psk_priv, psk_public, _, _, _ = ectx.create(ek_handle, TPM2B_SENSITIVE_CREATE(), tp.get_ps_key_templ(policyDigest))

		hierarchy.append((psk_priv, psk_public))

	end = time.time()
	print("[*]TIMING: Pseudonym creation takes", end-start)

	########################################################
	########################################################
	start = time.time()
	daa_handle, daa_public_key, daa_priv, daa_public, K1, Sch = init_join_phase(ek_handle)
	end = time.time()
	print("[*]TIMING: Join phase takes", end-start)


	# Socket
	arr = []
	arr.append(bytes(K1))
	data = pickle.dumps(Sch)
	arr.append(data)
	data = pickle.dumps(arr)

	coms.issuer_client(data)
	# End Socket

	print("[*]SOCKET: Waiting for isssuer's make_credential no2")

	# Socket
	c, addr = sd.accept()
	data = c.recv(4096)
	credblob, secret, ct_bytes = pickle.loads(data)

	c.close()
	# End Socket

	K2 = ectx.activate_credential(daa_handle, ek_handle, credblob, secret)

	cipher = AES.new(bytes(K2), AES.MODE_CFB, iv= b'\x00' * 16)
	pt = cipher.decrypt(ct_bytes)

	cre, s_cre = pickle.loads(pt)

	print("Finished tpm2_activate and decryption")

	curve = openssl_utils.get_BNP256_curve()

	openssl_utils.check_curve(curve)

	print("Host verifies credentials")
	start = time.time()
	host_verifies_AK_creds(curve, cre, s_cre)
	end = time.time()
	print("[*]TIMING: Credential verification takes", end-start)

	print("Finished tpm2_activate and decryption")

	print("DAA Signing phase")
	start = time.time()
	r_cre1, map_pt, J = openssl_utils.host_points_creation(curve, cre, daa_public_key)

	Scm, Points, message = daa_sign_phase(r_cre1, map_pt, J, daa_handle)
	end = time.time()
	print("[*]TIMING: DAA Signing phase takes", end-start)

	# Socket
	arr = []
	data = pickle.dumps(Scm)
	arr.append(data)

	data = pickle.dumps(Points)
	arr.append(data)

	data = pickle.dumps(message)
	arr.append(data)

	data_prime = pickle.dumps(arr)

	coms.issuer_client(data_prime)
	# End Socket

	print("[*]SOCKET: Waiting for isssuer's signature verification")

	ectx.FlushContext(daa_handle)

	########################################################
	# Now we send a Join request to RA
	########################################################

	Signed_CpHashes = []

	KH_EpK, EpK, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_ephemeral_key_templ(), primaryHandle = ESYS_TR.NULL)
	start = time.time()
	for ps in cpHash:

		scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
		scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
		validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

		temp = ectx.sign(KH_EpK, ps[0], scheme, validation)

		Signed_CpHashes.append((bytes(temp.signature.ecdsa.signatureR.buffer), bytes(temp.signature.ecdsa.signatureS.buffer)))
	end = time.time()
	print("[*]TIMING: Signing of cpHashes takes", end-start)

	ectx.FlushContext(KH_EpK)

	# Socket
	arr = []

	arr.append(policyDigests)
	# Only the Soft Revocation Signed cpHashes
	arr.append(Signed_CpHashes)

	data = pickle.dumps(arr)

	coms.RA_client(data)
	# End Socket

	print("[*]SOCKET: Waiting for RA to register signatures of CpHashes...")

	########################################################
	# Now we receive the cpSigs of the cpHashes of our keys from RA
	########################################################

	# Socket
	c, addr = sd.accept()

	data = c.recv(9182)

	SoftRevocationSigs, HardRevocationSigs = pickle.loads(data)
	
	c.close()
	# End Socket

	########################################################
	# Now we sign a msg with a pseudonym
	########################################################

	msg = b"{Speed: 20; Hdg: 180}"

	print("Now we sign a msg with the first pseudonym...")

	daa_handle = ectx.load(ek_handle, daa_priv, daa_public)

	pt_psk = (bytes(hierarchy[0][1].publicArea.unique.ecc.x), bytes(hierarchy[0][1].publicArea.unique.ecc.y))

	start = time.time()
	c, Points = psk_daa_sign(pt_psk, daa_handle, msg)

	ectx.FlushContext(daa_handle)

	ps_handle = ectx.load(ek_handle, hierarchy[0][0], hierarchy[0][1])

	signature = revocation_utils.sign_msg_with_peud(ectx, c, Revocation_handle, bits[0][0], bits[0][1], ps_handle, pt_psk, Points, msg)
	end = time.time()
	print("[*]TIMING: Message signing by pseudonym takes", end-start)

	ectx.FlushContext(ps_handle)

	########################################################
	########################################################


	########################################################
	# Now we sign a msg with daa
	########################################################

	# msg = b"{Speed: 40; Hdg: 200}"

	# print("Now we sign a msg with the daa...")

	# daa_handle = ectx.load(ek_handle, daa_priv, daa_public)

	# signature = revocation_utils.sign_msg_with_daa(ectx, msg, daa_handle)

	# ectx.FlushContext(daa_handle)


	########################################################
	# Now we revoke the 2nd pseudonym
	########################################################

	# data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	# counter = int.from_bytes(data.buffer, byteorder = "big")

	# print(bytes(data.buffer))

	# revocation_utils.revoke(ectx, sign_public, Revocation_handle, SoftRevocationSigs[1], policyDigests[1][0], policyDigests, cpHash, Beta, finalPolicy, 0, AKname, t_verified)
	# signature = revocation_utils.sign_msg_with_peud(ectx, c, Revocation_handle, bits[1][0], bits[1][1], ps_handle, pt_psk, Points, msg)

	# data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	# counter = int.from_bytes(data.buffer, byteorder = "big")

	# print(bytes(data.buffer))

	########################################################
	# Now we exec hard revoke for 1st 
	########################################################

	# data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	# print(bytes(data.buffer))
	# counter = int.from_bytes(data.buffer, byteorder = "big")
	# counter = "{0:064b}".format(counter)
	#print(counter)

	#revocation_utils.revoke(ectx, sign_public, Revocation_handle, SoftRevocationSigs[5], policyDigests[5][0], policyDigests, cpHash, allDigests, subDigests, finalPolicy, AKname, t_verified, 0)

	# data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	# # print(bytes(data.buffer))
	# counter = int.from_bytes(data.buffer, byteorder = "big")
	# counter = "{0:064b}".format(counter)
	# print(counter)

	# for i in range(pseudonyms):
	# 	revocation_utils.revoke(ectx, sign_public, Revocation_handle, SoftRevocationSigs[i], policyDigests[i][0], policyDigests, cpHash, allDigests ,subDigests, finalDigests, finalPolicy, AKname, t_verified, 0)
	# 	data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	# 	# print(bytes(data.buffer))
	# 	counter = int.from_bytes(data.buffer, byteorder = "big")
	# 	counter = "{0:064b}".format(counter)
	# 	print(counter)

	# revocation_utils.revoke(ectx, sign_public, Revocation_handle, HardRevocationSigs[20], policyDigests[20][1], policyDigests, cpHash, allDigests ,subDigests, finalDigests, finalPolicy, AKname, t_verified, 0)
	# data = ectx.NV_Read(Revocation_handle, 8, 0, ESYS_TR.RH_OWNER)
	# # print(bytes(data.buffer))
	# counter = int.from_bytes(data.buffer, byteorder = "big")
	# counter = "{0:064b}".format(counter)
	# print(counter)
		
	# revocation_utils.revoke(ectx, sign_public, Revocation_handle, HardRevocationSigs[7], policyDigests[7][1], policyDigests, cpHash, allDigests ,subDigests, finalPolicy, AKname, t_verified, 0)

	########################################################
	########################################################

	########################################################
	# Now we sign a msg with a pseudonym
	########################################################

	# msg = b"{Speed: 20; Hdg: 180}"

	# print("Now we sign a msg with the first pseudonym...")

	# daa_handle = ectx.load(ek_handle, daa_priv, daa_public)

	# pt_psk = (bytes(hierarchy[15][1].publicArea.unique.ecc.x), bytes(hierarchy[15][1].publicArea.unique.ecc.y))

	# start = time.time()
	# c, Points = psk_daa_sign(pt_psk, daa_handle, msg)

	# ectx.FlushContext(daa_handle)

	# ps_handle = ectx.load(ek_handle, hierarchy[10][0], hierarchy[10][1])

	# signature = revocation_utils.sign_msg_with_peud(ectx, c, Revocation_handle, bits[10][0], bits[10][1], ps_handle, pt_psk, Points, msg)
	# end = time.time()
	# print("[*]TIMING: Message signing by pseudonym takes", end-start)

	# ectx.FlushContext(ps_handle)

	########################################################
	########################################################

	end_timer = time.time()

	print("[*]TIMING: DAAV2X protocol takes", end_timer - prime_timer)

	sd.close()

##################################################################################################################
##################################################################################################################

from ctypes import cdll
from tpm2_pytss import *
from tpm2_pytss.types import *
import hashlib
import utils
import templates as tp
import pickle
import utils
import openssl_utils
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import revocation_utils
import communication_utils as coms
import socket
import time
import logging

logging.basicConfig(filename='car1testid.log',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

# logging.info("example")

def box2(data):
	try:
		sd_box2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		HOST1, PORT1 = "localhost", 9991

		sd_box2.connect((HOST1, PORT1))

		data = pickle.dumps(data)

		sd_box2.sendall(data)

		sd_box2.close()

	except:
		sd_box2.close()

def reboot(ectx):

	if(ectx != None):

		ectx.shutdown(TPM2_SU.STATE)
		logging.info("[*]STATE: TPM was successfully shutdowned")
		ectx.startup(TPM2_SU.CLEAR)
		logging.info("[*]STATE: TPM REBOOTED successfully")

		return ectx
	else:
		raise Exception("ERROR occured with the ectx context")

def pseud_reboot(ectx, ACI_handle, Revocation_handle, ek_handle):

	if(ectx != None):

		ectx.shutdown(TPM2_SU.CLEAR)
		logging.info("[*]STATE: TPM was successfully shutdowned")
		
		# ectx.close()
		# ectx = ESAPI(tcti)
		ectx.startup(TPM2_SU.CLEAR)
		ectx.NV_UndefineSpace(ACI_handle)
		ectx.NV_UndefineSpace(Revocation_handle)
		ectx.FlushContext(ek_handle)

		logging.info("[*]STATE: TPM REBOOTED successfully")

		return ectx
	else:
		raise Exception("ERROR occured with the ectx context")


def init_join_phase(ek_handle, sd_ISSUER, ek_public):

	logging.info("[*]KEYS: Creating daa key...")

	daa_priv, daa_public, _, _, _ = ectx.create(ek_handle, TPM2B_SENSITIVE_CREATE(), tp.get_daa_key_templ())

	daa_handle = ectx.load(ek_handle, daa_priv, daa_public)

	logging.info(f"[*]KEYS: The daa handle is at: {hex(daa_handle)}") 

	# Socket
	arr = []
	arr.append(ek_public.toPEM())
	arr.append(bytes(daa_public.getName()))
	arr.append(bytes(daa_public.publicArea.unique.ecc.x))
	arr.append(bytes(daa_public.publicArea.unique.ecc.y))

	data = pickle.dumps(arr)
	sd_ISSUER.connect((ISSUER_HOST, ISSUER_PORT))
	sd_ISSUER.sendall(data)
	# End Socket

	daa_public_key = (bytes(daa_public.publicArea.unique.ecc.x), bytes(daa_public.publicArea.unique.ecc.y))

	logging.info("[*]VERIFICATION: Waiting for issuer's make_credential")

	# Socket

	data = sd_ISSUER.recv(4096)
	cred, secret = pickle.loads(data)
	# End Socket

	K1 = ectx.activate_credential(daa_handle, ek_handle, cred, secret)

	pk_x, pk_y = utils.get_iso_point()

	string = b"".join(pk_x[0]) + b"".join(pk_x[1]) + b"".join(pk_y[0]) + b"".join(pk_y[1]) + bytes(K1)

	string += bytes(ek_public.publicArea.unique.rsa.buffer)

	_, _, E, counter = ectx.commit(daa_handle, TPM2B_ECC_POINT(), TPM2B_SENSITIVE_DATA(), TPM2B_ECC_PARAMETER())

	E = bytes(E.point.x.buffer) + bytes(E.point.y.buffer)

	logging.info(f"[*]TPM COMMAND: Commit was executed and the values are: {E=} {counter=}")
	
	p = hashlib.sha256(utils.get_bnp_P1() + b"".join(daa_public_key) + E + string).digest()

	p_tpm, validation = ectx.hash(p, TPM2_ALG.SHA256, ESYS_TR.ENDORSEMENT)

	logging.info("[*]TPM COMMAND: tpm2_hash finished!")

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDAA)
	scheme.details.ecdaa.hashAlg = TPM2_ALG.SHA256
	scheme.details.ecdaa.count = counter

	signature = ectx.sign(daa_handle, p_tpm, scheme, validation)
	nj = bytes(signature.signature.ecdaa.signatureR.buffer)
	w = bytes(signature.signature.ecdaa.signatureS.buffer)

	logging.info("[*]TPM COMMAND: tpm_sign finished!")

	temp_hash = hashlib.sha256(nj + bytes(p_tpm)).hexdigest()
	v = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	Sch = [v, w, nj]

	return daa_handle, daa_public_key, daa_priv, daa_public, K1, Sch


def host_verifies_AK_creds(curve, cre, s_cre, daa_public_key):

	(A, B, C, D) = cre

	(u, j) = s_cre

	p1 = (b"\x01", b"\x02")

	j_p1_bb = openssl_utils.ec_point_mul(curve, j, p1)
	res = openssl_utils.point_is_on_curve(curve, j_p1_bb)

	if (res != 1):
		raise Exception("[j]P1 is not on the curve")

	u_b_bb = openssl_utils.ec_point_mul(curve, u, B)
	res = openssl_utils.point_is_on_curve(curve, u_b_bb)

	if (res != 1):
		raise Exception("[u]B is not on the curve")

	tpm_bb = openssl_utils.ec_point_invert(curve, u_b_bb)
	Rb_prime_bb = openssl_utils.ec_point_add(curve, j_p1_bb, tpm_bb)
	res = openssl_utils.point_is_on_curve(curve, Rb_prime_bb)

	if (res != 1):
		raise Exception("Rb is not on the curve")

	j_qs_bb = openssl_utils.ec_point_mul(curve, j, daa_public_key)
	rc = openssl_utils.point_is_on_curve(curve, j_qs_bb)

	if (rc != 1):
		raise Exception("[j]Qs is not on curve")

	u_d_bb = openssl_utils.ec_point_mul(curve, u, D)
	rc = openssl_utils.point_is_on_curve(curve, u_d_bb)

	if (rc != 1):
		raise Exception("[u]D is not on curve")
		

	tpm_bb_prime = openssl_utils.ec_point_invert(curve, u_d_bb)

	Rd_prime_bb = openssl_utils.ec_point_add(curve, j_qs_bb, tpm_bb_prime)
	rc = openssl_utils.point_is_on_curve(curve, Rd_prime_bb)

	if (rc != 1):
		raise Exception("Rd is not on curve")

	p1 = utils.get_bnp_P1()

	u_prime = hashlib.sha256(p1 + b"".join(daa_public_key) + b"".join(A) + b"".join(B) + b"".join(C) + b"".join(D) + b"".join(Rb_prime_bb) + b"".join(Rd_prime_bb)).digest()

	if (u == u_prime):
		logging.info("[*]VERIFICATION: Verified AK Creds OK")
	else:
		raise SystemExit("JOIN failed")


def daa_sign_phase(r_cre1, map_pt, J, daa_handle):

	pt_s = r_cre1[1]

	p1 = TPM2B_ECC_POINT(TPMS_ECC_POINT(x = pt_s[0], y = pt_s[1]))
	s2 = TPM2B_SENSITIVE_DATA(map_pt[0])
	y2 = TPM2B_ECC_PARAMETER(map_pt[1])

	K, L, E, counter = ectx.commit(daa_handle, p1, s2, y2)

	logging.info("[*]TPM COMMAND: tpm2_commit for DAA Sign finished!")

	# R,S,T,W = r_cre1
	Points = r_cre1
	Points.append(J)

	K = (bytes(K.point.x), bytes(K.point.y))
	Points.append(K)
	L = (bytes(L.point.x), bytes(L.point.y))
	Points.append(L)
	E = (bytes(E.point.x), bytes(E.point.y))
	Points.append(E)

	message = b"Speed 70 mph, traffic light"
	md = hashlib.sha256(message).digest()

	c = hashlib.sha256(md + b"".join(b"".join(p) for p in Points)).digest()

	h1, validation = ectx.hash(c, TPM2_ALG.SHA256, ESYS_TR.ENDORSEMENT)

	logging.info("[*]TPM COMMAND: tpm2_hash for DAA Sign finished!")

	scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDAA)
	scheme.details.ecdaa.hashAlg = TPM2_ALG.SHA256
	scheme.details.ecdaa.count = counter

	signature = ectx.sign(daa_handle, h1, scheme, validation)

	logging.info("[*]TPM COMMAND: tpm2_sign for DAA Sign finished!")

	nm = bytes(signature.signature.ecdaa.signatureR.buffer)
	s = bytes(signature.signature.ecdaa.signatureS.buffer)

	temp_hash = hashlib.sha256(nm + bytes(h1)).hexdigest()
	h2 = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

	Scm = [h2, s, nm]

	Points = Points[:-2] # Here we are removing the L and E points cause these must be recreated by issuer for verification

	return Scm, Points, message



def psk_daa_sign(ps_public_key, daa_handle, msg):

	# ps_public_key = (bytes(ps_public.publicArea.unique.ecc.x), bytes(ps_public.publicArea.unique.ecc.y))

	nonce_random_bytes = get_random_bytes(4 * 32)

	temp = nonce_random_bytes[:32]
	r = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	temp = nonce_random_bytes[32:64]
	x = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	temp = nonce_random_bytes[64:96]
	y = utils.int_to_bytes(int.from_bytes(temp, "big") % utils.BNP256_ORDER)

	p1 = (b"\x01", b"\x02")
	cre = []

	curve = openssl_utils.get_BNP256_curve()

	openssl_utils.check_curve(curve)

	cre.append(openssl_utils.ec_point_mul(curve, r, p1)) #point A
	cre.append(openssl_utils.ec_point_mul(curve, y, cre[0])) #point B

	ry = utils.int_to_bytes((int.from_bytes(r, "big") * int.from_bytes(y, "big")) % utils.BNP256_ORDER)

	temp = openssl_utils.ec_point_mul(curve, ry, ps_public_key)

	cre.append(openssl_utils.ec_point_mul(curve, x, temp)) #point C
	cre.append(temp) #point D


	r_cre1, map_pt, J = openssl_utils.host_points_creation(curve, cre, ps_public_key)

	pt_s = r_cre1[1]

	p1 = TPM2B_ECC_POINT(TPMS_ECC_POINT(x = pt_s[0], y = pt_s[1]))
	s2 = TPM2B_SENSITIVE_DATA(map_pt[0])
	y2 = TPM2B_ECC_PARAMETER(map_pt[1])

	K, L, E, counter = ectx.commit(daa_handle, p1, s2, y2)

	logging.info("[*]TPM COMMAND: tpm2_commit for pseud Sign finished!")

	# R,S,T,W = r_cre1
	Points = r_cre1
	Points.append(J)

	K = (bytes(K.point.x), bytes(K.point.y))
	Points.append(K)
	L = (bytes(L.point.x), bytes(L.point.y))
	Points.append(L)
	E = (bytes(E.point.x), bytes(E.point.y))
	Points.append(E)
	
	md = hashlib.sha256(msg).digest()

	c = hashlib.sha256(md + b"".join(b"".join(p) for p in Points)).digest()

	Points = Points[:-2]

	return c, Points



cdll.LoadLibrary("libtss2-tcti-mssim.so")

tcti = TctiLdr("mssim", f"port=2321,host=172.0.0.1")

pseudonyms = 63
nvCounterIndex = 0x01010101

ISSUER_HOST, ISSUER_PORT = "localhost", 9999

sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print("[*]SOCKET: Socket successfully created")

HOST, PORT = "localhost", 9998

sd.bind((HOST, PORT))

print("[*]SOCKET: Socket binded successfully")

sd.listen()

print("[*]SOCKET: Box is listening...")

#############################

RA_HOST, RA_PORT = "localhost", 9995

TPM_status = 0



with ESAPI(tcti) as ectx:

	ectx = ESAPI(tcti)

	ectx.startup(TPM2_SU.CLEAR)

	prime_timer = time.time()

	# data = b"Ready"

	def start_join(ectx):

		sd_ISSUER = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# Added for new 63 pseud
		sd_RA = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		# print("Creating endorsement key...")

		hierarchy = []

		ek_handle, ek_public, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_endorsement_key_templ(), primaryHandle=ESYS_TR.ENDORSEMENT)

		daa_handle, daa_public_key, daa_priv, daa_public, K1, Sch = init_join_phase(ek_handle, sd_ISSUER, ek_public)
		hierarchy.append((daa_priv, daa_public))

		# Socket
		arr = []
		arr.append(bytes(K1))
		data = pickle.dumps(Sch)
		arr.append(data)
		data = pickle.dumps(arr)

		# check if we need the returned value
		sd_ISSUER.sendall(data)
		# End Socket

		logging.info("[*]VERIFICATION: Waiting for isssuer's make_credential no2")

		# Socket
		data = sd_ISSUER.recv(4096)
		credblob, secret, ct_bytes = pickle.loads(data)
		# End Socket

		K2 = ectx.activate_credential(daa_handle, ek_handle, credblob, secret)

		cipher = AES.new(bytes(K2), AES.MODE_CFB, iv= b'\x00' * 16)
		pt = cipher.decrypt(ct_bytes)

		cre, s_cre = pickle.loads(pt)

		logging.info("[*]TPM COMMAND: Finished tpm2_activate and decryption")

		curve = openssl_utils.get_BNP256_curve()

		openssl_utils.check_curve(curve)

		logging.info("[*]VERIFICATION: Host verifies credentials")
		start = time.time()
		host_verifies_AK_creds(curve, cre, s_cre, daa_public_key)
		end = time.time()
		print("[*]TIMING: Credential verification takes", end-start)

		logging.info("[*]TPM COMMAND: Finished tpm2_activate and decryption")

		logging.info("[*]VERIFICATION: DAA Signature verification phase")
		start = time.time()
		r_cre1, map_pt, J = openssl_utils.host_points_creation(curve, cre, daa_public_key)

		Scm, Points, message = daa_sign_phase(r_cre1, map_pt, J, daa_handle)
		end = time.time()
		print("[*]TIMING: DAA Signing phase takes", end-start)

		# Socket
		arr = []
		data = pickle.dumps(Scm)
		arr.append(data)

		data = pickle.dumps(Points)
		arr.append(data)

		data = pickle.dumps(message)
		arr.append(data)

		data_prime = pickle.dumps(arr)

		sd_ISSUER.sendall(data_prime)
		# End Socket

		logging.info("[*]VERIFICATION: Waiting for isssuer's signature verification")

		# Socket
		data = sd_ISSUER.recv(4096)
		verified = pickle.loads(data)
		# End Socket

		if (verified == "False"):
			raise SystemExit("[*]VERIFICATION: DAA Verification from issuer failed")

		logging.info("[*]VERIFICATION: Vehicle 1 is Trusted Entity")

		ectx.FlushContext(daa_handle)

		sd_RA.connect((RA_HOST, RA_PORT))

		# Added for new 63 pseud
		data = ["sign_key"]
		data = pickle.dumps(data)
		sd_RA.sendall(data)

		########################

		# Here we integrate the revocation initialize phase until we have a revocation handle ready 

		logging.info("[*]INIT_REVOCATION_UTILS: Creating authorization counter index...")
		start = time.time()
		ACI_handle = revocation_utils.auth_counter_index(ectx, nvCounterIndex)
		end = time.time()
		print("[*]TIMING: Authorization counter index creation takes", end-start)

		# TODO: Reboot of tpm and check that ACI works the same

		ectx = reboot(ectx)

		logging.info("[*]INIT_REVOCATION_UTILS: Creating revocation index...")
		start = time.time()
		Revocation_handle = revocation_utils.init_revocation_index(ectx, ACI_handle)
		end = time.time()
		print("[*]TIMING: Revocation index creation takes", end-start)
		########################################################
		########################################################

		logging.info("[*]INIT_REVOCATION_UTILS: Loading RA's signing key...")

		# Socket
		# c, addr = sd.accept()
		data = sd_RA.recv(4096)
		sd_RA.close()

		signing_key_public = pickle.loads(data)
		# End Socket

		sign_public = tp.load_sign_ra_pem(signing_key_public)

		signing_key_name = sign_public.getName()

		bits = []
		for i in range(pseudonyms):
			bits.append((i+1, 0))

		#######################################################################
		# We recreate AK handle for using it for activating revocation handle
		#######################################################################

		aci_public, aci_name = ectx.NV_ReadPublic(ACI_handle)

		Pd = hashlib.sha256(hashlib.sha256(bytearray(32)+b"\x00\x00\x01\x51"+bytes(aci_name)).digest()).digest()

		inP = tp.get_ephemeral_key_templ()
		inP.publicArea.authPolicy = Pd

		KH_AK, AK_pub, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), inP)

		########################################################
		########################################################

		logging.info("[*]INIT_REVOCATION_UTILS: Generating final policy digest...")
		start = time.time()
		finalPolicy, name_final_rev, allDigests, cpHash, policyDigests, subDigests, finalDigests = revocation_utils.get_final_pol_digest(ectx, Revocation_handle, AK_pub, signing_key_name, bits)

		end = time.time()
		print("[*]TIMING: Generating final policy takes", end-start)

		logging.info("[*]INIT_REVOCATION_UTILS: Activating revocation index...")
		start = time.time()
		AKname, t_verified = revocation_utils.activate_revoc_index(ectx, finalPolicy, name_final_rev, KH_AK, AK_pub, finalDigests, Revocation_handle, ACI_handle)

		end = time.time()
		print("[*]TIMING: Activating revocation index takes", end-start)

		logging.info("[*]INIT_REVOCATION_UTILS: Finished Revocation utils initialization..")

		logging.info(f"[*]KEYS: Now we create " + str(pseudonyms) + " pseudonym keys...")

		########################################################
		# now we create our pseudonyms!
		########################################################

		start = time.time()
		for i in range(pseudonyms):

			policyDigest = revocation_utils.getPolicyNVDigest(ectx, bits[i][0], bits[i][1], Revocation_handle)

			psk_priv, psk_public, _, _, _ = ectx.create(ek_handle, TPM2B_SENSITIVE_CREATE(), tp.get_ps_key_templ(policyDigest))

			hierarchy.append((psk_priv, psk_public))

		end = time.time()
		print("[*]TIMING: Pseudonym creation takes", end-start)

		########################################################
		########################################################

		public_parts = {}
		for i in range(pseudonyms+1):
			public_parts.update({i: bytes(hierarchy[i][1].publicArea.unique.ecc.y)})

		return public_parts, cpHash, policyDigests, hierarchy, ek_handle, Revocation_handle, bits, sign_public, finalPolicy, AKname, t_verified, allDigests, subDigests, finalDigests, ACI_handle

	########################################################
	# Now we send a Join request to RA
	########################################################

	def activate_keys(cpHash, policyDigests, hierarchy):

		logging.info("[*]ACTIVATION: Vehicle sign the CpHashes")

		Signed_CpHashes = []

		KH_EpK, EpK, _, _, _ = ectx.CreatePrimary(TPM2B_SENSITIVE_CREATE(), tp.get_ephemeral_key_templ(), primaryHandle = ESYS_TR.NULL)
		start = time.time()
		for ps in cpHash:

			scheme = TPMT_SIG_SCHEME(scheme=TPM2_ALG.ECDSA)
			scheme.details.ecdsa.hashAlg = TPM2_ALG.SHA256
			validation = TPMT_TK_HASHCHECK(tag=TPM2_ST.HASHCHECK, hierarchy=TPM2_RH.NULL)

			temp = ectx.sign(KH_EpK, ps[0], scheme, validation)

			Signed_CpHashes.append((bytes(temp.signature.ecdsa.signatureR.buffer), bytes(temp.signature.ecdsa.signatureS.buffer)))
		end = time.time()
		print("[*]TIMING: Signing of cpHashes takes", end-start)

		ectx.FlushContext(KH_EpK)

		pseud_public = []

		for i in range(pseudonyms):
			# psk_pub = bytes(hierarchy[i][1].publicArea.unique.ecc.y)
			pseud_public.append(bytes(hierarchy[i+1][1].publicArea.unique.ecc.y))
		# Socket
		sd_RA = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sd_RA.connect((RA_HOST, RA_PORT))

		arr = []

		arr.append(pseud_public)

		arr.append(policyDigests)
		# Only the Soft Revocation Signed cpHashes
		arr.append(Signed_CpHashes)

		data = pickle.dumps(arr)

		sd_RA.sendall(data)
		# End Socket

		########################################################
		# Now we receive the cpSigs of the cpHashes of our keys from RA
		########################################################

		# Socket
		data = sd_RA.recv(18364)

		logging.info("[*]ACTIVATION: Vehicles received from RA the signatured CpHashes...")

		SoftRevocationSigs, HardRevocationSigs = pickle.loads(data)
		# End Socket

		activated = {}

		for i in range(pseudonyms):
			# psk_pub = bytes(hierarchy[i][1].publicArea.unique.ecc.y)
			activated.update({bytes(hierarchy[i+1][1].publicArea.unique.ecc.y): ((SoftRevocationSigs[i], HardRevocationSigs[i]))})

		logging.info("[*]ACTIVATION: Pseudonyms are activated...")
		return activated

	########################################################
	# Now we sign a msg with a pseudonym
	########################################################

	def push_msg(msg, ps_no):

		msg = msg.encode()

		ps_sign = ps_no % pseudonyms

		logging.info("[*]BROADCASTING: Now we sign a msg with pseudonym...")

		daa_priv, daa_public = hierarchy[0]
		daa_handle = ectx.load(ek_handle, daa_priv, daa_public)

		pt_psk = (bytes(hierarchy[ps_sign+1][1].publicArea.unique.ecc.x), bytes(hierarchy[ps_sign+1][1].publicArea.unique.ecc.y))

		start = time.time()
		c, Points = psk_daa_sign(pt_psk, daa_handle, msg)

		ectx.FlushContext(daa_handle)

		ps_handle = ectx.load(ek_handle, hierarchy[ps_sign+1][0], hierarchy[ps_sign+1][1])

		signature = revocation_utils.sign_msg_with_peud(ectx, c, Revocation_handle, bits[ps_sign][0], bits[ps_sign][1], ps_handle, pt_psk, Points, msg)
		end = time.time()
		print("[*]TIMING: Message signing by pseudonym takes", end-start)

		ectx.FlushContext(ps_handle)
		while (signature == -1):

			logging.info(f"[*]BROADCASTING: The pseudonym" + str(ps_sign) + "is revoked and can't be ued for signing")

			ps_no = ps_no + 1
			ps_sign = ps_sign + 1
			
			pt_psk = (bytes(hierarchy[ps_sign+1][1].publicArea.unique.ecc.x), bytes(hierarchy[ps_sign+1][1].publicArea.unique.ecc.y))
		
			daa_handle = ectx.load(ek_handle, daa_priv, daa_public)
			
			c, Points = psk_daa_sign(pt_psk, daa_handle, msg)
			ectx.FlushContext(daa_handle)

			ps_handle = ectx.load(ek_handle, hierarchy[ps_sign+1][0], hierarchy[ps_sign+1][1])
			signature = revocation_utils.sign_msg_with_peud(ectx, c, Revocation_handle, bits[ps_sign][0], bits[ps_sign][1], ps_handle, pt_psk, Points, msg)
			ectx.FlushContext(ps_handle)

		sig_r = bytes(signature.signature.ecdaa.signatureR.buffer)
		sig_s = bytes(signature.signature.ecdaa.signatureS.buffer)

		logging.info(f"[*]BROADCASTING: We have just signed a msg with the pseudonym --> " + str(pt_psk[1]) + "that has signature --> " + str(sig_s))

		temp_hash = hashlib.sha256(sig_r).hexdigest()
		h2 = utils.int_to_bytes(int(temp_hash, 16) % utils.BNP256_ORDER)

		Scm = [h2, sig_s, sig_r]
		ps_no += 1

		return c, Scm, Points, ps_no, ps_sign, pt_psk


	def message_verification(Scm, message, Points, pt_psk):

		curve = openssl_utils.get_BNP256_curve()

		openssl_utils.check_curve(curve)

		rc = openssl_utils.point_is_on_curve(curve, pt_psk)
		if (rc != 1):
			raise Exception("The ECDSA public key is not on the curve")

		daa_verified = openssl_utils.psk_sign_verification(curve, pt_psk, Scm, message)

		# daa_verified = openssl_utils.psk_DAA_verify(curve, Scm, message, Points)

		return daa_verified


	########################################################
	########################################################

	ps_no = 0

	while True:
		c, addr = sd.accept()

		flag = c.recv(4096)

		data = pickle.loads(flag)


		if(data[0] == "start"):

			verified, cpHash, policyDigests, hierarchy, ek_handle, Revocation_handle, bits, sign_public, finalPolicy, AKname, t_verified, allDigests, subDigests, finalDigests, ACI_handle = start_join(ectx)

			arr = []
			arr.append(verified)
			data = pickle.dumps(arr)
			c.sendall(data)
			c.close()
			#Flask_socket(data)

		elif(data[0] == "activate"):

			activated = activate_keys(cpHash, policyDigests, hierarchy)

			arr = []
			arr.append(activated)
			data = pickle.dumps(arr)
			c.sendall(data)
			c.close()


		elif(data[0] == "push"):
			message = data[1]
			Sigtype = data[2]
			
			if(TPM_status == 0):
				encrypt, Scm, Points, ps_no, ps_sign, pt_psk = push_msg(message, ps_no)
				if(Sigtype == "1"):
					Scm[1] += b"\x00\x50\x00\x01" 

				arr = []

				data = pickle.dumps(encrypt)
				arr.append(data)

				data = pickle.dumps(Scm)
				arr.append(data)

				data = pickle.dumps(Points)
				arr.append(data)

				data = pickle.dumps(message)
				arr.append(data)

				data = pickle.dumps(bytes(hierarchy[ps_sign + 1][1].publicArea.unique.ecc.y))
				arr.append(data)

				data = pickle.dumps(pt_psk)
				arr.append(data)

				data_prime = pickle.dumps(arr)

				box2(data_prime)

				flask = ["Message broadcasted successfully"]
				flask.append(bytes(hierarchy[ps_sign + 1][1].publicArea.unique.ecc.y))

				flask = pickle.dumps(flask)

				c.sendall(flask)
				
				c.close()
			else:
				logging.info("[*]BROADCASTING: Vehicle was hard revoked and cannot sign messages anymore")

				flask = ["TPM was hard revoked"]
				
				flask = pickle.dumps(flask)

				c.sendall(flask)
				
				c.close()


		elif(data[0] == "revoke"):

			revocationSig = data[1]

			pol_digest = data[2]

			revoke_status, TPM_status = revocation_utils.revoke(ectx, sign_public, Revocation_handle, revocationSig, pol_digest, policyDigests, cpHash, allDigests, subDigests, finalDigests, finalPolicy, AKname, t_verified, TPM_status)

			if(revoke_status == 1):

				if (TPM_status == 0):
					logging.info("[*]REVOKE: Pseudonym was revoked successfully")
				else:
					logging.info("[*]REVOKE: Vehicle was hard revoked")

				flask = ["Pseudonym revoked"]
				flask = pickle.dumps(flask)
				c.sendall(flask)
				c.close()
			else:
				logging.info("[*]REVOKE: Revocation request is not for us")
				flask = ["Pseudonym Was not for Us"]
				flask = pickle.dumps(flask)
				c.sendall(flask)
				c.close()

		# Added for new 63 pseud

		elif(data[0] == "reboot"):

			ps_no = 0
			nvCounterIndex += 1

			ectx = pseud_reboot(ectx, ACI_handle, Revocation_handle, ek_handle)
			TPM_status = 0
			logging.info("[*]STATE: TPM was successfully shutdowned and rebooted")

			status = ["rebooted successfully"]
			status = pickle.dumps(status)
			c.sendall(status)
			c.close()

		##########################


		else:

			msg_to_digest, Scm_prime, Points_prime, message_prime, pub_pseud, pt_psk = pickle.loads(data)

			msg_to_digest = pickle.loads(msg_to_digest)
			Scm = pickle.loads(Scm_prime)
			Points = pickle.loads(Points_prime)
			message = pickle.loads(message_prime)
			pseudo = pickle.loads(pub_pseud)
			pt_psk = pickle.loads(pt_psk)

			daa_verified = message_verification(Scm, msg_to_digest, Points, pt_psk)
			if(daa_verified == True):

				logging.info("[*]BROADCASTING: Message received, and verified")
			else:

				logging.warning(f"[*]BROADCASTING: Message " + str(message) + " received, but signature: " + str(Scm[1]) + " is not valid for pseudonym: " + str(pseudo))



	########################################################
	########################################################

	end_timer = time.time()

	print("[*]TIMING: DAAV2X protocol takes", end_timer - prime_timer)

	# sd.close()

##################################################################################################################
##################################################################################################################

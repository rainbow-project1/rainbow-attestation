
/** \file Create_key.h
 * Header file of the key managament functionality
 */
#ifndef CREATEKEY_H
#define CREATEKEY_H

#include <iostream>
#include <cstring>
#include <string>
#include <ibmtss/tss.h>
#include <ibmtss/tssresponsecode.h>
/* https://trustedcomputinggroup.org/wp-content/uploads/TCG_TPM2_r1p59_Part2_Structures_pub.pdf - Table 27 */
const uint32_t ekPersistentNVindexHandle=0x810100c0;

/**
 * @brief This function create a primary RSA-key under a fixed policy
 * @param tssContext the context to the TPM
 * @param hierarchy Key hierarchy for the Key, e.g. Endorsement
 * @param out output values for the key-generation from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC createPrimaryRsaKey(TSS_CONTEXT *tssContext,	TPMI_RH_HIERARCHY hierarchy,	CreatePrimary_Out* out);
/**
 * @brief This function create a primary ECC-key under a fixed policy
 * @param tssContext the context to the TPM
 * @param hierarchy Key hierarchy for the Key, e.g. Endorsement
 * @param out output values for the key-generation from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC createPrimaryEccKey(TSS_CONTEXT *tssContext,	TPMI_RH_HIERARCHY hierarchy,	CreatePrimary_Out* out);
/**
 * @brief This function create an ECDSA-key under a fixed policy and a primary key
 * @param tssContext the context to the TPM
 * @param parentKeyHandle Handle of the previous generated parent key
 * @param curveID ID of an ECC curve
 * @param out output values for the key-generation from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC createEcdsaKey(TSS_CONTEXT* tssContext,  TPM_HANDLE parentKeyHandle, TPMI_ECC_CURVE curveID, Create_Out* out);
/**
 * @brief This function create an ECDAA-key under a fixed policy and a primary key
 * @param tssContext the context to the TPM
 * @param parentKeyHandle Handle of the previous generated parent key
 * @param curveID ID of an ECC curve
 * @param out output values for the key-generation from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC createDaaKey( TSS_CONTEXT* tssContext,	 TPM_HANDLE parentKeyHandle, TPMI_ECC_CURVE curveID, Create_Out* out);
/**
 * @brief This function writes a key in the NV memory, i.e. persisten key
 * @param tssContext the context to the TPM
 * @param auth Authentication of the key, e.g. Owner
 * @param keyHandle Key-handle of the key that should be written into the NV (out.objectHandle)
 * @param persistentKeyHandle Key-Handle of the primary key under which the key lies
 * @return returns if the createn of the key was successful or not
 */
TPM_RC makeKeyPersistent(TSS_CONTEXT* tssContext, TPMI_RH_PROVISION auth, TPM_HANDLE keyHandle, TPM_HANDLE persistentKeyHandle);
/**
 * @brief This function deletes a key from the NV memory
 * @param tssContext the context to the TPM
 * @param authentication Authentication of the key, e.g. Owner
 * @param persistentHandle Key-Handle of the primary key under which the key lies
 * @return returns if the createn of the key was successful or not
 */
TPM_RC deletePersistentKey(TSS_CONTEXT *tssContext,  TPMI_RH_PROVISION authentication, TPM_HANDLE persistentHandle);
/**
 * @brief This function loads a child key into the TPM. Note: a create key functions does not use the load function automatically in contrast to create a primary key
 * @param tssContext the context to the TPM
 * @param parentHandle Key-Handle of the primary key under which the key lies
 * @param outKey output values (structures) of the child key
 * @param loadOut output values of the load-key-function from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC loadKey(TSS_CONTEXT *tssContext, TPM_HANDLE parentHandle, Create_Out outKey, Load_Out *loadOut);
/**
 * @brief This function loads a child key into the TPM. Note: a create key functions does not use the load function automatically in contrast to create a primary key
 * @param tssContext the context to the TPM
 * @param parentHandle Key-Handle of the primary key under which the key lies
 * @param outKey output values (structures) of the child key
 * @param loadOut output values of the load-key-function from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC flushContext(TSS_CONTEXT* tssContext, TPMI_DH_CONTEXT keyHandle);
/**
 * @brief This function loads an external key into the TPM.
 * @param tssContext the context to the TPM
 * @param hierarchy Key hierarchy for the Key, e.g. Endorsement
 * @param keyPublic public portion of the key
 * @param loadOut output values of the load-key-function from the TPM
 * @return returns if the createn of the key was successful or not
 */
TPM_RC loadExternalKey(TSS_CONTEXT *tssContext, TPMI_RH_HIERARCHY hierarchy,  TPM2B_PUBLIC keyPublic, LoadExternal_Out *loadOut);
std::string parseTpmError(TPM_RC rc);
#endif /*CREATEKEY_H*/

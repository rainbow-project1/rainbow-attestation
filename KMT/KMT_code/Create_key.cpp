/** \file Create_key.cpp
 * Key managament functionality code
 */

#include <sstream>
#include <cstring>
#include "Create_key.h"

/** 
 * @mainpage KMT code
 * @section intro_sec Introduction
 * This file offers key-management TPM functionalities
 * @section install_sec Installation
 * call make
 * @subsection install_dependencies Installing Dependencies
 * Install the TPM TSS stack (see rainbow_tpm_autoinstallation) and enable the Hardware TPM on the Raspberry Pi
 */


static const int iwgPolicy[32] = {
    0x83, 0x71, 0x97, 0x67, 0x44, 0x84, 0xB3, 0xF8, 0x1A, 0x90, 0xCC,
    0x8D, 0x46, 0xA5, 0xD7, 0x24, 0xFD, 0x52, 0xD7, 0x6E, 0x06, 0x52,
    0x0B, 0x64, 0xF2, 0xA1, 0xDA, 0x1B, 0x33, 0x14, 0x69, 0xAA
};

TPM_RC createPrimaryRsaKey( TSS_CONTEXT* tssContext, TPMI_RH_HIERARCHY hierarchy, CreatePrimary_Out* out)
{
  
  TPM_RC rc = TPM_RC_BAD_TAG;
  CreatePrimary_In in;
  uint32_t tpmaObject = TPMA_OBJECT_FIXEDTPM |
                        TPMA_OBJECT_FIXEDPARENT |
                        TPMA_OBJECT_SENSITIVEDATAORIGIN |
                        TPMA_OBJECT_USERWITHAUTH |
                        TPMA_OBJECT_RESTRICTED |
                        /*TPMA_OBJECT_NODA |*/
                        TPMA_OBJECT_DECRYPT;

  /* https://trustedcomputinggroup.org/wp-content/uploads/TPM-Rev-2.0-Part-2-Structures-01.38.pdf - p.61*/
  

  in.primaryHandle = hierarchy;
  in.inSensitive.sensitive.userAuth.t.size = 0;
  in.inSensitive.sensitive.data.t.size = 0;

  in.outsideInfo.t.size = 0;
  in.creationPCR.count = 0;
  
  /* Default EK RSA 2048 Template https://trustedcomputinggroup.org/wp-content/uploads/TCG_IWG_Credential_Profile_EK_V2.1_R13.pdf - p36 */
  TPMT_PUBLIC &tpmtPublic=in.inPublic.publicArea;
  tpmtPublic.type = TPM_ALG_RSA;
  tpmtPublic.nameAlg = TPM_ALG_SHA256;
  tpmtPublic.objectAttributes.val = tpmaObject;
  tpmtPublic.authPolicy.t.size =  sizeof(iwgPolicy)/sizeof(iwgPolicy[0]);
  memcpy(tpmtPublic.authPolicy.t.buffer, &iwgPolicy[0], sizeof(iwgPolicy)/sizeof(iwgPolicy[0]) );
  tpmtPublic.parameters.rsaDetail.symmetric.algorithm = TPM_ALG_AES;
  tpmtPublic.parameters.rsaDetail.symmetric.keyBits.aes = 128;
  tpmtPublic.parameters.rsaDetail.symmetric.mode.aes=TPM_ALG_CFB;
  tpmtPublic.parameters.rsaDetail.scheme.scheme = TPM_ALG_NULL;
  tpmtPublic.parameters.rsaDetail.keyBits = 2048;
  tpmtPublic.parameters.rsaDetail.exponent = 0;
  //tpmtPublic.parameters.rsaDetail.exponent = 65537/*0*/;
  //tpmtPublic.parameters.rsaDetail.exponent = 65537;
  tpmtPublic.unique.rsa.t.size = 0;
  ReadClock_Out out1;
  ReadClock_Out out2;
  rc = TSS_Execute(tssContext,
			 (RESPONSE_PARAMETERS *)&out1, 
			 NULL,
			 NULL,
			 TPM_CC_ReadClock,
			 TPM_RH_NULL, NULL, 0);
  rc = TSS_Execute( tssContext,
                    (RESPONSE_PARAMETERS *)out,
                    (COMMAND_PARAMETERS *)&in,
                    NULL,
                    TPM_CC_CreatePrimary,
                    TPM_RS_PW, NULL, 0,         // Password auth., no password
                    TPM_RH_NULL, NULL, 0        // End of list of session 3-tuples
                  );  
  if (rc != TPM_RC_SUCCESS)
  {
    printf("Error CreatePrimary\n");
    std::cout << "Error CreatePrimary:\n" << parseTpmError(rc) << "\n";
    return -1;
  }
  rc = TSS_Execute(tssContext,
			 (RESPONSE_PARAMETERS *)&out2, 
			 NULL,
			 NULL,
			 TPM_CC_ReadClock,
			 TPM_RH_NULL, NULL, 0);
  //ReadClock_Out diff = out2.currentTime-out1.currentTime;
  	TSS_TPMS_TIME_INFO_Print(&out2.currentTime, 0);
    TSS_TPMS_TIME_INFO_Print(&out1.currentTime, 0);
    printf("\t%lld-%lld\n",out2.currentTime.time,out1.currentTime.time);
  //printf("createPrimaryRsaKey: time: %llu",diff.currentTime); //PRIu64
 
  
  return rc;
}

TPM_RC createPrimaryEccKey(TSS_CONTEXT *tssContext,	TPMI_RH_HIERARCHY hierarchy,	CreatePrimary_Out* out)
{
  TPM_RC rc = TPM_RC_BAD_TAG;
  CreatePrimary_In in;
  uint32_t tpmaObject = TPMA_OBJECT_FIXEDTPM |
                        TPMA_OBJECT_FIXEDPARENT |
                        TPMA_OBJECT_SENSITIVEDATAORIGIN |
                        TPMA_OBJECT_USERWITHAUTH |
                        TPMA_OBJECT_RESTRICTED |
                        /*TPMA_OBJECT_NODA |*/
                        TPMA_OBJECT_DECRYPT;

  /* https://trustedcomputinggroup.org/wp-content/uploads/TPM-Rev-2.0-Part-2-Structures-01.38.pdf - p.61*/
  

  in.primaryHandle = hierarchy;
  in.inSensitive.sensitive.userAuth.t.size = 0;
  in.inSensitive.sensitive.data.t.size = 0;

  in.outsideInfo.t.size = 0;
  in.creationPCR.count = 0;
  
  /* Default EK RSA 2048 Template https://trustedcomputinggroup.org/wp-content/uploads/TCG_IWG_Credential_Profile_EK_V2.1_R13.pdf - p36 */
  TPMT_PUBLIC &tpmtPublic=in.inPublic.publicArea;
  tpmtPublic.type = TPM_ALG_ECC;
  tpmtPublic.nameAlg = TPM_ALG_SHA256;
  tpmtPublic.objectAttributes.val = tpmaObject;
  tpmtPublic.authPolicy.t.size =  sizeof(iwgPolicy)/sizeof(iwgPolicy[0]);
  memcpy(tpmtPublic.authPolicy.t.buffer, &iwgPolicy[0], sizeof(iwgPolicy)/sizeof(iwgPolicy[0]) );
  tpmtPublic.parameters.eccDetail.symmetric.algorithm = TPM_ALG_AES;
  tpmtPublic.parameters.eccDetail.symmetric.keyBits.aes = 128;
  tpmtPublic.parameters.eccDetail.symmetric.mode.aes=TPM_ALG_CFB;
  tpmtPublic.parameters.eccDetail.scheme.scheme = TPM_ALG_NULL;
  tpmtPublic.parameters.eccDetail.curveID = TPM_ECC_NIST_P256;
  tpmtPublic.parameters.eccDetail.kdf.scheme = TPM_ALG_NULL;


  tpmtPublic.unique.ecc.x.t.size = 0;
	tpmtPublic.unique.ecc.y.t.size = 0;


  rc = TSS_Execute( tssContext,
                    (RESPONSE_PARAMETERS *)out,
                    (COMMAND_PARAMETERS *)&in,
                    NULL,
                    TPM_CC_CreatePrimary,
                    TPM_RS_PW, NULL, 0,         // Password auth., no password
                    TPM_RH_NULL, NULL, 0        // End of list of session 3-tuples
                  );  

  if (rc != TPM_RC_SUCCESS)
  {
    printf("Error CreatePrimary\n");
    std::cout << "Error CreatePrimary:\n" << parseTpmError(rc) << "\n";
  }

  return rc;
}

TPM_RC createEcdsaKey(TSS_CONTEXT* tssContext, TPM_HANDLE parentKeyHandle, TPMI_ECC_CURVE curveID, Create_Out* out)
{
  TPM_RC rc = TPM_RC_BAD_TAG;
  Create_In in;	
  uint32_t tpmaObject = 0;
  if(curveID == TPM_ECC_BN_P256)
  {
    printf("BN_P256 is not supported by the TPM\n");
    return -1;
  }
  tpmaObject =  TPMA_OBJECT_FIXEDTPM |
                /*TPMA_OBJECT_NODA |*/
                TPMA_OBJECT_FIXEDPARENT |
                TPMA_OBJECT_SENSITIVEDATAORIGIN |
                TPMA_OBJECT_USERWITHAUTH |
                TPMA_OBJECT_SIGN;
  
	in.parentHandle = parentKeyHandle;
  in.inSensitive.sensitive.userAuth.t.size = 0;
	in.inSensitive.sensitive.data.t.size = 0;
  in.outsideInfo.t.size = 0;
	in.creationPCR.count = 0;

	TPMT_PUBLIC& tpmtPublic = in.inPublic.publicArea;
	tpmtPublic.type = TPM_ALG_ECC;
	tpmtPublic.nameAlg = TPM_ALG_SHA256;
	tpmtPublic.objectAttributes.val = tpmaObject;

  /* no policy */
  tpmtPublic.authPolicy.t.size=0;
  tpmtPublic.parameters.eccDetail.symmetric.algorithm = TPM_ALG_NULL;
  tpmtPublic.parameters.eccDetail.scheme.scheme = TPM_ALG_ECDSA;
	tpmtPublic.parameters.eccDetail.scheme.details.ecdsa.hashAlg = TPM_ALG_SHA256;
  tpmtPublic.parameters.eccDetail.curveID = curveID;
  /* todo check */
  tpmtPublic.parameters.eccDetail.kdf.scheme = TPM_ALG_NULL;

  tpmtPublic.unique.ecc.x.t.size = 0;
	tpmtPublic.unique.ecc.y.t.size = 0;
  rc = TSS_Execute( tssContext,
                    (RESPONSE_PARAMETERS *)out,
                    (COMMAND_PARAMETERS *)&in,
                    NULL,
                    TPM_CC_Create,
                    TPM_RS_PW, NULL, 0,
                    TPM_RH_NULL, NULL, 0
                  );

	if (rc != TPM_RC_SUCCESS)
	{
		printf("Error CreateECDSAkey \n");
	}	
  
  return rc;
  
}



TPM_RC createDaaKey(TSS_CONTEXT* tssContext, TPM_HANDLE parentKeyHandle, TPMI_ECC_CURVE curveID, Create_Out* out)
{
  TPM_RC rc = TPM_RC_BAD_TAG;
  Create_In in;
  uint32_t tpmaObject = 0;
 

  in.parentHandle = parentKeyHandle;
	/* Table 133 - Definition of TPMS_SENSITIVE_CREATE Structure <IN>sensitive  */
	/* Table 75 - Definition of Types for TPM2B_AUTH userAuth */
	in.inSensitive.sensitive.userAuth.t.size = 0;
	in.inSensitive.sensitive.data.t.size = 0;
	
	
	TPMT_PUBLIC& tpmtPublic = in.inPublic.publicArea;
	tpmtPublic.type = TPM_ALG_ECC;
	tpmtPublic.nameAlg = TPM_ALG_SHA256;


  tpmaObject =  TPMA_OBJECT_FIXEDTPM |
                /*TPMA_OBJECT_NODA |*/
                TPMA_OBJECT_FIXEDPARENT |
                TPMA_OBJECT_SENSITIVEDATAORIGIN |
                TPMA_OBJECT_USERWITHAUTH |
                TPMA_OBJECT_SIGN;
  tpmtPublic.objectAttributes.val = tpmaObject;
  tpmtPublic.parameters.eccDetail.symmetric.algorithm = TPM_ALG_NULL;

	tpmtPublic.parameters.eccDetail.scheme.scheme = TPM_ALG_ECDAA;
	tpmtPublic.parameters.eccDetail.scheme.details.ecdaa.hashAlg = TPM_ALG_SHA256;
	tpmtPublic.parameters.eccDetail.scheme.details.ecdaa.count = 1;
	tpmtPublic.parameters.eccDetail.curveID = curveID;
	tpmtPublic.parameters.eccDetail.kdf.scheme = TPM_ALG_NULL;

  tpmtPublic.unique.ecc.x.t.size = 0;
	tpmtPublic.unique.ecc.y.t.size = 0;

	tpmtPublic.authPolicy.t.size=0;

	in.outsideInfo.t.size = 0;

  in.creationPCR.count = 0;

	rc = TSS_Execute( tssContext,
                    (RESPONSE_PARAMETERS *)out,
                    (COMMAND_PARAMETERS *)&in,
                    NULL,
                    TPM_CC_Create,
                    TPM_RS_PW, NULL, 0,
                    TPM_RH_NULL, NULL, 0
                  );

  if (rc != TPM_RC_SUCCESS)
	{
    std::cout << "Error CreateDAAkey: " << parseTpmError(rc) << "\n";
	}	
  return rc;

}


TPM_RC makeKeyPersistent(TSS_CONTEXT* tssContext, TPMI_RH_PROVISION auth, TPM_HANDLE keyHandle, TPM_HANDLE persistentKeyHandle)
{ 
  TPM_RC  rc = TPM_RC_BAD_TAG, rc1;
  EvictControl_In in;
  
  in.auth = auth;
  in.objectHandle = keyHandle;
  in.persistentHandle = persistentKeyHandle;
  ReadClock_Out out1;
  ReadClock_Out out2;
  rc1 = TSS_Execute(tssContext,
			 (RESPONSE_PARAMETERS *)&out1, 
			 NULL,
			 NULL,
			 TPM_CC_ReadClock,
			 TPM_RH_NULL, NULL, 0);
  rc = TSS_Execute( tssContext,
                    NULL, 
                    (COMMAND_PARAMETERS *)&in,
                    NULL,
                    TPM_CC_EvictControl,
                    TPM_RS_PW, NULL, 0,
                    TPM_RH_NULL, NULL, 0
                  );
 
  if (rc != TPM_RC_SUCCESS)
  {            
    std::cout << "Error makeKeyPersistent: \n" << parseTpmError(rc) << "\n";
  }
  rc1 = TSS_Execute(tssContext,
			 (RESPONSE_PARAMETERS *)&out2, 
			 NULL,
			 NULL,
			 TPM_CC_ReadClock,
			 TPM_RH_NULL, NULL, 0);
  //ReadClock_Out diff = out2.currentTime-out1.currentTime;
  	TSS_TPMS_TIME_INFO_Print(&out2.currentTime, 0);
    TSS_TPMS_TIME_INFO_Print(&out1.currentTime, 0);
    printf("\t%lld-%lld\n",out2.currentTime.time,out1.currentTime.time);
  return rc;
}

TPM_RC deletePersistentKey(TSS_CONTEXT *tssContext, TPMI_RH_PROVISION auth, TPM_HANDLE persistentHandle)
{
  TPM_RC  rc = makeKeyPersistent(tssContext,auth, persistentHandle, persistentHandle);
  if(rc != TPM_RC_SUCCESS)
  {
    printf("Error EvictControl deletePersistent Key\n");
  }
  return rc;
}

TPM_RC flushContext(TSS_CONTEXT* tssContext, TPMI_DH_CONTEXT keyHandle)
{
  TPM_RC  rc = TPM_RC_BAD_TAG;
  
  FlushContext_In in;
  in.flushHandle=keyHandle;
  
  rc = TSS_Execute(  tssContext,
                      NULL, 
                      (COMMAND_PARAMETERS *)&in,
                      NULL,
                      TPM_CC_FlushContext,
                      TPM_RH_NULL, NULL, 0
                    );
    if (rc != TPM_RC_SUCCESS)
    {
       printf("Error flushContext\n");
    }
    return rc;
}

TPM_RC loadKey(TSS_CONTEXT *tssContext, TPM_HANDLE parentHandle,  Create_Out outKey, Load_Out *loadOut)
{
  TPM_RC rc = TPM_RC_BAD_TAG;
  Load_In loadIn;

  loadIn.parentHandle = parentHandle;
	loadIn.inPrivate = (TPM2B_PRIVATE) outKey.outPrivate;
	loadIn.inPublic = (TPM2B_PUBLIC) outKey.outPublic;
  rc = TSS_Execute( tssContext,
                    (RESPONSE_PARAMETERS *)loadOut,
                    (COMMAND_PARAMETERS *)&loadIn,
                    NULL,
                    TPM_CC_Load,
                    TPM_RS_PW, NULL, 0,
                    TPM_RH_NULL, NULL, 0
                  );

  if (rc != TPM_RC_SUCCESS)
  {
     std::cout << "Error LoadKey: \n" << parseTpmError(rc) << "\n";
  }
  return rc;
}

TPM_RC loadExternalKey(TSS_CONTEXT *tssContext, TPMI_RH_HIERARCHY hierarchy,  TPM2B_PUBLIC keyPublic, LoadExternal_Out *loadOut)
{
  TPM_RC rc = TPM_RC_BAD_TAG;
  LoadExternal_In loadIn;

  loadIn.inPublic = keyPublic;
  loadIn.inPrivate.t.size = 0;
  loadIn.hierarchy = hierarchy;
  rc = TSS_Execute(tssContext,
                  (RESPONSE_PARAMETERS *)loadOut,
                  (COMMAND_PARAMETERS *)&loadIn,
                  NULL,
                  TPM_CC_LoadExternal,
                  TPM_RH_NULL, NULL, 0); /* No authorisation session */

  if (rc != TPM_RC_SUCCESS)
  {
     std::cout << "Error LoadExternalKey: \n" << parseTpmError(rc) << "\n";
  }
  return rc;
}

std::string parseTpmError(TPM_RC rc)
{
    std::ostringstream os;
    const char *msg;
    const char *submsg;
    const char *num;
	
    TSS_ResponseCode_toString(&msg, &submsg, &num, rc);
    os << msg << ", " << submsg << ", " << num;

    return os.str();
}
var searchData=
[
  ['loadexternalkey_0',['loadExternalKey',['../_create__key_8cpp.html#a023081b592e7dd862d19d9528ca28db7',1,'loadExternalKey(TSS_CONTEXT *tssContext, TPMI_RH_HIERARCHY hierarchy, TPM2B_PUBLIC keyPublic, LoadExternal_Out *loadOut):&#160;Create_key.cpp'],['../_create__key_8h.html#a023081b592e7dd862d19d9528ca28db7',1,'loadExternalKey(TSS_CONTEXT *tssContext, TPMI_RH_HIERARCHY hierarchy, TPM2B_PUBLIC keyPublic, LoadExternal_Out *loadOut):&#160;Create_key.cpp']]],
  ['loadkey_1',['loadKey',['../_create__key_8cpp.html#a5ddbd8f273116d0c6eae9ddf93a4b6f0',1,'loadKey(TSS_CONTEXT *tssContext, TPM_HANDLE parentHandle, Create_Out outKey, Load_Out *loadOut):&#160;Create_key.cpp'],['../_create__key_8h.html#a5ddbd8f273116d0c6eae9ddf93a4b6f0',1,'loadKey(TSS_CONTEXT *tssContext, TPM_HANDLE parentHandle, Create_Out outKey, Load_Out *loadOut):&#160;Create_key.cpp']]]
];


/** \file keytestmain.cpp
 * Functionality test cases as an example
 */
#include "Create_key.h"
#include <chrono>
#include <fstream>

int main()
{
    TPM_RC rc = 0;
    TSS_CONTEXT* tssContext=nullptr;
    CreatePrimary_Out outPrimary;
    Create_Out outDAA;
    //Create_Out outEcdsa;
   
    Startup_In in;
    
    rc = TSS_SetProperty(NULL,TPM_TRACE_LEVEL,"2");
    if(rc != TPM_RC_SUCCESS)
    {
      return -1;
    }
    rc = TSS_Create(&tssContext);
    if(rc != TPM_RC_SUCCESS)
    {
      return -1;
    }
    rc = TSS_SetProperty(tssContext, TPM_INTERFACE_TYPE, "dev");
    
    if(rc != TPM_RC_SUCCESS)
    {
      return -1;
    }
    #if 0
    std::ofstream myfile;
    myfile.open ("eccprimarykey.txt");
    for( int i = 0; i < 10; i++)
    {
    
    do 
    {
      std::cout << '\n' << "PRESS a key to continue...";
    } while (std::cin.get() != '\n'); 
    #endif
    in.startupType = TPM_SU_CLEAR;
    
    rc = TSS_Execute(tssContext,
                    NULL, 
                    (COMMAND_PARAMETERS *)&in,
                    NULL,
                    TPM_CC_Startup,
                    TPM_RH_NULL, NULL, 0);
    if(rc == TPM_RC_INITIALIZE)
    {
      printf("reset the TPM module\n");
      return -1;
    }
    
   
     
      #if 0 
      auto start = std::chrono::high_resolution_clock::now();
      #endif
      rc = createPrimaryRsaKey(tssContext,TPM_RH_ENDORSEMENT,&outPrimary);
     // rc = createPrimaryEccKey(tssContext,TPM_RH_ENDORSEMENT,&outPrimary);
      #if 0
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      #endif
      //printf("HERE\n");
      //scanf("%d",&rc);
    if( rc != TPM_RC_SUCCESS)
    {
      printf("create primary faild\n");
      return -1;
    }
    else
    {
      printf("Primary Key created\n");
    }
     auto start = std::chrono::high_resolution_clock::now();
    rc = makeKeyPersistent(tssContext,TPM_RH_OWNER,outPrimary.objectHandle,ekPersistentNVindexHandle);
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    printf("%ld\n",duration);
    if(rc == TPM_RC_NV_DEFINED)
    {
      rc = deletePersistentKey(tssContext,TPM_RH_OWNER,ekPersistentNVindexHandle);
      if(rc == TPM_RC_SUCCESS)
      {
        rc = makeKeyPersistent(tssContext,TPM_RH_OWNER,outPrimary.objectHandle,ekPersistentNVindexHandle);
        if(rc != TPM_RC_SUCCESS)
        {
          return -1;
        }
      }
    }
    if(rc != TPM_RC_SUCCESS && rc != TPM_RC_NV_DEFINED)
    {
      return -1;
    }
    rc = createDaaKey(tssContext,ekPersistentNVindexHandle ,TPM_ECC_BN_P256,&outDAA);
    if(rc != TPM_RC_SUCCESS)
    {
      return -1;
    }
    else{
      printf("Create DAA Key \n");
    }
    Load_Out loadOut;
    rc = loadKey(tssContext, ekPersistentNVindexHandle,outDAA, &loadOut);
    if(rc != TPM_RC_SUCCESS)
    {
      return -1;
    }
    else{
      printf("Loaded DAA Key \n");
    }
      //createEcdsaKey(tssContext,outPrimary.objectHandle,TPM_ECC_NIST_P256,&outEcdsa);
    #if 0
    rc = deletePersistentKey(tssContext,TPM_RH_OWNER,ekPersistentNVindexHandle);
    if(rc != TPM_RC_SUCCESS)
    {
      return -1;
    }
    #endif
    #if 0
    myfile << duration.count() << "\n";
    }
    myfile.close();
    #endif
    return 0;
}
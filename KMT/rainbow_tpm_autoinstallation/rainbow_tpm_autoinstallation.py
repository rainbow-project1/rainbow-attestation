"""
Copyright 2022 Infineon Technolgies Austria AG
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Author: Raphael Schermann
"""

import os
import sys
import getopt
import subprocess
import tarfile
#import argparse


class bcolors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def download_ibm_tss():
    print("download ibmtss1.6.0")

    filename = "ibmtss_latest.tar.gz"

    if not os.path.exists(filename):
        os.system("wget -O "+filename+" https://sourceforge.net/projects/ibmtpm20tss/files/latest/download")
        with tarfile.open(filename) as zip_ref:
            zip_ref.extractall("./ibmtss")

    print(bcolors.GREEN + "Downloading Completed"+ bcolors.ENDC)

def install_dependencies():
    print("install dependencies")
    p1 = subprocess.run("sudo apt-get -y install openssl",shell = True)
    p2 = subprocess.run("sudo apt-get -y install libssl-dev",shell = True)
    print(bcolors.GREEN +"dependencies installed" + bcolors.ENDC)

def tpm2_found():
    if os.path.exists("/dev/tpm0"):
        p1 = subprocess.run("dmesg",shell = True,capture_output= True)
        print(p1.stdout.decode())
        if p1.stdout.decode().find("tpm0"):
            print("tpm0 module found")
            return 0
        else:
            print("no tpm0 module found")
            return -1
    else:
        print("no tpm0 module found")
    return -1

def tpm2_check():
    p1 = subprocess.run("git clone https://github.com/Infineon/eltt2.git",shell = True)
    os.chdir("eltt2")
    p1 = subprocess.run("make -j4",shell = True)
    p1 = subprocess.run("sudo ./eltt2 -t",shell = True,capture_output= True)
    if(p1.stdout.decode().find("Successfully tested. Works as expected.")):
        print("TPM module successfully tested. Works as expected.")
        os.chdir("../")
        return 0
    else:
        print(p1.stdout.decode())
        os.chdir("../")
        return -1
def tpm2_make_install_tpm():
    os.chdir("ibmtss")
    p1 = subprocess.run("autoreconf -i",shell = True)
    p1 = subprocess.run("./configure",shell = True)
    p1 = subprocess.run("make clean",shell = True)
    p1 = subprocess.run("make",shell = True)
    p1 = subprocess.run("sudo make install",shell = True)
    print(bcolors.GREEN +"tss successfully installed"+ bcolors.ENDC)
    os.chdir("../")
    p1 = subprocess.run("sudo cp -R ibmtss /opt/",shell = True)
    p1 = subprocess.run("sudo chmod 755 -R /opt/ibmt*", shell = True)
if __name__ == "__main__":
    install_dependencies()
    if tpm2_found() == -1:
        print("no tpm found")
        exit(1)
    if tpm2_check() == -1:
        print("tpm check failed")
        exit(1)
    download_ibm_tss()
    tpm2_make_install_tpm()
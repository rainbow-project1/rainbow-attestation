# TPM autoinstallation

Script to install a Hardware TPM with the use of the IBM's TSS stack.

## Raspberry Pi Setup

1. Enable ssh and/or VNC
For ssh create a file called ssh in the root directory of the SD card
2. Enable SPI
```
dtparam=spi=on
dtoverlay=tpm-slb9670
```

### Prerequisites
Hardware TPM (SLB9670), python
### Installing (automatically)
To install the HW-TPM TSS stack call

```
python3 tpm_autoinstallation_ibm.py
```

### Installing (manually)
Dowload the TSS stack: https://sourceforge.net/projects/ibmtpm20tss/files/latest/download
```
autoreconf -i
./configure
make
sudo make install
```

## Disclaimer

All implementations are only research prototypes.

## License
MIT License (MIT)

Copyright (c) 2022 Infineon Technolies Austria AG

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
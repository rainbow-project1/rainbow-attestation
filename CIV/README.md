# RAINBOW's Configuration Integrity Verification (CIV) component

This repository contains the source code of the component that runs on edge devices (**provers**) and on the Orchestrator (the centralized **verifier**). To aid in deployment, each project contains the necessary configuration files and documentation for deploying that project as a container instance. Specifically, the project for deploying provers is located [here](prover) along with its [documentation](prover/README.md) and the project for deploying the centralized Orchestrator container is located [here](prover) along with its [documentation](orchestrator/README.md).

## Architecture

Once provers and the Orchestrator are deployed, **Secure Enrollment** of provers happens through the REST Application Programming Interface (API) that is exposed on the Orchestrator. The following diagram illustrates the communication between the prover and Orchestrator containers.

![alt text](SecureEnrollment.png "Title Text")

Upon enrollment requests, the Orchestrator runs the **Secure Enrollment Protocol**, which includes secure Attestation Key (AK) establishment, certifiable measurements (update), and **Oblivious Remote Attestation (ORA)** of a specified prover.

## Disclaimer

All implementations are only research prototypes.

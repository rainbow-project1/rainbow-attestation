#include "handler.h"

handler::handler(utility::string_t url, const bool verbose) : listener(url), verbose(verbose) {
    listener.support(methods::GET, std::bind(&handler::handle_get, this, std::placeholders::_1));
    listener.support(methods::POST, std::bind(&handler::handle_post, this, std::placeholders::_1));
}

void handler::handle_get(http_request message) {
    if (verbose) ucout << message.to_string() << endl;
    message.reply(status_codes::NotFound);
};

void handler::handle_post(http_request message) {

    // extract JSON object from request
    json::value temp;
    message.extract_json()
        .then([&temp](pplx::task<json::value> task) {
        temp = task.get();
    }).wait();

    // print request headers and JSON body
    if (verbose) {
        ucout << U(message.method().c_str()) << U(" ") << message.request_uri().path() << endl;
        for (const auto& header: message.headers()) {
            ucout << header.first + U(": ") + header.second << endl;
        }

        utility::string_t body = temp.serialize();
        if (body.compare(U("null")) != 0) {
            ucout << endl << body << endl << endl;
        } else {
            ucout << endl;
        }
    }

    json::value response;

    auto path = uri::split_path(uri::decode(message.relative_uri().path()));

    if (path[0] == "enroll") {

        // extract and decode data from JSON object
        utility::string_t url  = temp.at(U("proverApi")).as_string();
        utility::string_t fqpn = temp.at(U("fqpn")).as_string();
        utility::string_t digestBase64 = temp.at(U("digest")).as_string();

        vector<unsigned char> digestStr = utility::conversions::from_base64(digestBase64);

        /* START OF demo variables */
        // UINT32 pcr = 2;
        UINT32 pcr = 0x01001500;
        bool nv = true;
        // bool nv = false;
        unsigned char secretHmacKey[4] = {0x00, 0x00, 0x13, 0x37}; // shared HMAC key
        /* END OF demo variables */

        if (this->verbose) printf("[Info] AK creation on %s\n", url.c_str());

        if (this->orc->deploy(url)) {
            if (this->verbose) printf("[Info] Succesfull AK creation verification of container at %s\n\n", url.c_str());
        } else {
            if (this->verbose) printf("[Error] Failed to securely enroll container at %s\n\n", url.c_str());
            response[U("msg")] = json::value::string(U("Failed to establish AK on prover"));
            message.reply(status_codes::InternalError, response);
            return;
        }

        if (this->verbose) printf("[Info] Attaching PCR to %s\n", url.c_str());

        if (this->orc->addPcr(url, pcr, nv)) {
            if (nv) {
                if (this->verbose) printf("[Info] Added NV-based PCR %d to container at %s\n\n", pcr, url.c_str());
            } else {
                if (this->verbose) printf("[Info] Added PCR %d to container at %s\n\n", pcr, url.c_str());
            }
        } else {
            if (nv) {
                if (this->verbose) printf("[Error] Failed to add NV-based PCR to prover\n\n");
                response[U("msg")] = json::value::string(U("Failed to add NV-based PCR to prover"));
            } else {
                if (this->verbose) printf("[Error] Failed to add PCR to prover\n\n");
                response[U("msg")] = json::value::string(U("Failed to add PCR to prover"));
            }
            message.reply(status_codes::InternalError, response);
            return;
        }

        if (this->verbose) printf("[Info] Updating %s\n", url.c_str());

        if (this->orc->update(url, pcr, nv, &digestStr[0], fqpn, secretHmacKey, 4)) {
            if (this->verbose) printf("[Info] Succesfully updated container at %s\n\n", url.c_str());
        } else {
            if (this->verbose) printf("[Error] Failed to update container at %s\n\n", url.c_str());
            response[U("msg")] = json::value::string(U("Failed to update prover"));
            message.reply(status_codes::InternalError, response);

            if (this->orc->delPcr(url, pcr, nv)) {
                if (nv) {
                    if (this->verbose) printf("[Info] Removed NV-based PCR %d from container at %s\n\n", pcr, url.c_str());
                } else {
                    if (this->verbose) printf("[Info] Removed PCR %d from container at %s\n\n", pcr, url.c_str());
                }
            } else {
                if (nv) {
                    if (this->verbose) printf("[Error] Failed to remove NV-based PCR %d from container at %s\n\n", pcr, url.c_str());
                } else {
                    if (this->verbose) printf("[Error] Failed to remove PCR %d from container at %s\n\n", pcr, url.c_str());
                }
            }
            return;
        }

        if (this->verbose) printf("[Info] Attesting %s\n", url.c_str());

        // Oblivious Remote Attestation (ORA), i.e., verification with only knowledge about the prover's certified public key
        if (this->orc->attest(url)) {
            if (this->verbose) printf("[Info] Container at %s is in a correct state\n\n", url.c_str());
        } else {
            if (this->verbose) printf("[Info] Container at %s is in an incorrect state\n\n", url.c_str());
            response[U("msg")] = json::value::string(U("Attestation failed"));
            message.reply(status_codes::InternalError, response);

            if (this->orc->delPcr(url, pcr, nv)) {
                if (nv) {
                    if (this->verbose) printf("[Info] Removed NV-based PCR %d from container at %s\n\n", pcr, url.c_str());
                } else {
                    if (this->verbose) printf("[Info] Removed PCR %d from container at %s\n\n", pcr, url.c_str());
                }
            } else {
                if (nv) {
                    if (this->verbose) printf("[Error] Failed to remove NV-based PCR %d from container at %s\n\n", pcr, url.c_str());
                } else {
                    if (this->verbose) printf("[Error] Failed to remove PCR %d from container at %s\n\n", pcr, url.c_str());
                }
            }
            return;
        }

        response[U("msg")] = json::value::string(U("Enrolled"));
        message.reply(status_codes::OK, response);

        if (this->verbose) printf("[Info] Removing attached PCR from %s\n", url.c_str());

        if (this->orc->delPcr(url, pcr, nv)) {
            if (nv) {
                if (this->verbose) printf("[Info] Removed NV-based PCR %d from container at %s\n\n", pcr, url.c_str());
            } else {
                if (this->verbose) printf("[Info] Removed PCR %d from container at %s\n\n", pcr, url.c_str());
            }
        } else {
            if (nv) {
                if (this->verbose) printf("[Error] Failed to remove NV-based PCR %d from container at %s\n\n", pcr, url.c_str());
            } else {
                if (this->verbose) printf("[Error] Failed to remove PCR %d from container at %s\n\n", pcr, url.c_str());
            }
        }
    } else {
        message.reply(status_codes::NotFound);
    }
};

# Attestation Protocol Verification
This repository holds the models reated for verification of the security protocols used in RAINBOW.

## Description

ProVerif models of the RAINBOW security model.


## Disclaimer

All implementations are only research prototypes.

## License

Copyright (c) 2022 Politecnico di Torino

